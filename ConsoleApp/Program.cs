﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Net.Sockets;
using Assets.Scripts.Model.Logic;
using System.Timers;

namespace ConsoleApp
{
    class Program
    {
        public const Int32 UpdateRate = 300;

        private GameModel GameModel { get; set; }
        private Timer GameTimer { get; set; }
        private DateTime LastUpdate { get; set; }
        private String LocalIP { get; set; }
        private Int32 LocalPort { get; set; }

        public Program()
        {
            GameModel = new GameModel();
            
            GameTimer = new Timer();
            GameTimer.Interval = UpdateRate;

            GameModel.InstructionChanged += Model_InstructionChanged;
            GameModel.MachineStateChanged += Model_MachineStateChanged;
            GameModel.GameInitialized += Model_GameInitialized;
            GameModel.GameStarted += Model_GameStarted;
            GameModel.GameEnded += GameModel_GameEnded;
            GameModel.RaftStateChanged += Model_RaftStateChanged;
            GameModel.PlayerConnected += Model_PlayerConnected;
            GameModel.PlayerDisconnected += Model_PlayerDisconnected;
            GameModel.RaftLeaderChanged += Model_RaftLeaderChanged;
            GameTimer.Elapsed += GameTimer_Elapsed;
        }

        public void Run()
        {
            Console.WriteLine("1. Join Room");
            Console.WriteLine("2. Create Room");
            Int32 option = ReadInteger();
            
            if (option == 1)
                RunJoinRoom();
            else if (option == 2)
                RunCreateRoom();
        }

        private Int32 ReadInteger()
        {
            Int32 option;
            while (!Int32.TryParse(Console.ReadLine(), out option)) ;

            return option;
        }

        private void RunConfig()
        {
            Console.WriteLine("Local Port:");
            LocalPort = ReadInteger();

            Console.WriteLine("Found local addresses:");
            List<String> addresses = SocketAddress.LocalIPs;
            for (Int32 i = 0; i < addresses.Count; i++)
                Console.WriteLine("{0} - {1}", i, addresses[i]);

            Int32 option = ReadInteger();
            LocalIP = addresses[option];
        }

        private void RunJoinRoom()
        {
            RunConfig();

            Console.WriteLine("Remote Address:");
            String remoteAddress = Console.ReadLine();

            Console.WriteLine("Remote Port:");
            Int32 remotePort = ReadInteger();

            GameModel.Initialize(LocalIP, LocalPort, GameModel.DefaultInstructionsFilename);
            GameModel.JoinRoom(remoteAddress, remotePort);

            RunGame();
        }


        private void RunCreateRoom()
        {
            RunConfig();

            GameModel.Initialize(LocalIP, LocalPort, GameModel.DefaultInstructionsFilename);
            Console.WriteLine("Address/Port: {0}/{1}", LocalIP, LocalPort);

            RunGame();
        }

        private void RunGame()
        {
            while (true)
            {
                String command = Console.ReadLine();
                if (command == "exit")
                    break;

                else if (command == "button")
                    GameModel.ButtonClicked("button");

                else if (command == "initialize")
                    GameModel.InitializeGame();

                else if (command == "start")
                    GameModel.StartGame();        
            }
        }

        private void Model_RaftLeaderChanged(object sender, Assets.Scripts.Model.Raft.PersistentState.LeaderChangedEventArgs e)
        {
            Console.WriteLine("New Leader: {0}", e.LeaderID);
        }

        private void Model_InstructionChanged(object sender, Assets.Scripts.Model.Logic.GameLogic.InstructionsChangedEventArgs e)
        {
            Console.WriteLine("Instruction: {0}", e.Name);
        }

        private void Model_MachineStateChanged(object sender, Assets.Scripts.Model.Raft.PersistentState.MachineStateChangedEventArgs e)
        {
            Console.WriteLine("Command: {0}", e.Command);
        }

        private void Model_GameInitialized(object sender, EventArgs e)
        {
            Console.WriteLine("Game initialized!");
        }

        private void Model_GameStarted(object sender, EventArgs e)
        {
            LastUpdate = DateTime.Now;

            // Start raising Elapsed event:
            GameTimer.Start();

            Console.WriteLine("Game started!");
        }

        private void GameTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimeSpan deltaTime = e.SignalTime - LastUpdate;
            LastUpdate = e.SignalTime;

            Int32 deltaMilliseconds = Convert.ToInt32(deltaTime.TotalMilliseconds);

            GameModel.Update(deltaMilliseconds);
        }

        private void GameModel_GameEnded(object sender, Assets.Scripts.Model.Logic.GameLogic.GameEndedEventArgs e)
        {
            GameTimer.Stop();

            if (e.Won)
                Console.WriteLine("Game won!");
            else
                Console.WriteLine("Game lost!");
        }

        private void Model_PlayerConnected(object sender, Assets.Scripts.Model.Net.ServerEventArgs e)
        {
            Console.WriteLine("Player connected: {0}", e.ServerID);
        }

        private void Model_PlayerDisconnected(object sender, Assets.Scripts.Model.Net.ServerEventArgs e)
        {
            Console.WriteLine("Player disconnected: {0}", e.ServerID);
        }

        private void Model_RaftStateChanged(object sender, Assets.Scripts.Model.Raft.RaftServer.StateChangedEventArgs e)
        {
            Console.WriteLine("Raft State changed to {0}, CurrentTerm: {1}", e.State.ToString(), e.CurrentTerm);
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.Run();
        }
    }
}
