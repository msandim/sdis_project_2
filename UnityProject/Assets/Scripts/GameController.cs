﻿using Assets.Scripts.Model;
using Assets.Scripts.Model.Net;
using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameController : MonoBehaviour
    {
        public static readonly String InstructionsDictionaryFilename = @"Assets\Resources\InstructionsDictionary.txt";

        public static GameController Instance { get; private set; }

        public GameModel GameModel { get; private set; }

        public void Start()
        {
            Instance = this;

            GameModel = new GameModel();
        }

        public void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void Update()
        {
            // Calculate delta time in milliseconds:
            Int32 deltaMilliseconds = Convert.ToInt32(Time.deltaTime * 1000.0f);

            GameModel.Update(deltaMilliseconds);
        }
    }
}
