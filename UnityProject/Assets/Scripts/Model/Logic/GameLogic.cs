﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Assets.Scripts.Model.Logic
{
    using Net;
    using Net.Messages;
    using Messages;
    using Raft;

    public class GameLogic
    {
        public class GameInitializedEventArgs : EventArgs
        {
            public HashSet<String> Buttons { get; private set; }

            public GameInitializedEventArgs(HashSet<String> buttons)
            {
                Buttons = buttons;
            }
        }
        public class GameEndedEventArgs : EventArgs
        {
            public Boolean Won { get; private set; }

            public GameEndedEventArgs(Boolean won)
            {
                Won = won;
            }
        }
        public class InstructionsChangedEventArgs : EventArgs
        {
            public String Name { get; private set; }

            public InstructionsChangedEventArgs(String name)
            {
                Name = name;
            }
        }

        public event EventHandler<GameInitializedEventArgs> GameInitialized;
        public event EventHandler GameStarted;
        public event EventHandler<GameEndedEventArgs> GameEnded;
        public event EventHandler<InstructionsChangedEventArgs> InstructionChanged;

        private RaftServer Raft { get; set; }
        private INetwork Network { get; set; }
        private Boolean IsGameInitialized { get; set; }
        public Boolean IsGameRunning { get; private set; }
        private Random Random { get; set; }
        private Scheduler Scheduler { get; set; }
        private Int32 MaxRightMoves { get; set; }
        private Int32 MaxWrongMoves { get; set; }
        private Int32 NumberButtons { get; set; } //total number of buttons in the screen

        /* ******************* GAME STATE **************** */

        /// <summary>
        /// Names of the instructions - assure they are not repeated.
        /// </summary>
        private HashSet<String> AllInstructions { get; set; }

        /// <summary>
        /// Screens buttons from all the palyers
        /// </summary>
        private Dictionary<String, HashSet<String>> CurrentPlayersButtons { get; set; }

        /// <summary>
        /// Screen buttons of player.
        /// </summary>
        public HashSet<String> CurrentButtons { get; private set; }

        public String CurrentInstruction { get; private set; }
        public TimeSpan StartTime { get; private set; }
        public Int32 InstructionElapsedTime { get; set; }
        public Int32 InstructionTimeout { get; private set; }
        private Int32 RightMoves { get; set; }
        private Int32 WrongMoves { get; set; }

        private object _lockObject = new object();

        public GameLogic()
        {
            Random = new Random();
            Scheduler = new Scheduler();
        }

        public void Initialize(String dictionaryFilename, RaftServer raftServer, INetwork network)
        {
            // Read instructions from file:
            AllInstructions = new HashSet<String>(File.ReadAllLines(dictionaryFilename));

            // Initialize variables:
            NumberButtons = 6;
            Raft = raftServer;
            Network = network;

            // Subscribe to events:
            Raft.MachineStateChanged += Raft_MachineStateChanged;
            Network.ServerDisconnected += Network_ServerDisconnected;
            Network.DataReceived += Network_DataReceived;
        }

        public void Shutdown()
        {
            // Unsubscribe from events:
            Network.DataReceived -= Network_DataReceived;
            Network.ServerDisconnected -= Network_ServerDisconnected;
            Raft.MachineStateChanged -= Raft_MachineStateChanged;
        }

        /// <summary>
        /// Ensures that the game state is initialized across all players.
        /// Must be called before starting a new game.
        /// </summary>
        public void InitializeGame()
        {
            lock (this)
            {
                Int32 numberRightMoves = 10;
                Int32 numberWrongMoves = 10;

                // Send initialize game message.
                // If this message is committed, then the game can be started.
                SendInitializeGameMessages(numberRightMoves, numberWrongMoves);
            }
        }

        /// <summary>
        /// Ensures that all players begin at the same time.
        /// Must be called after the game is initialized.
        /// </summary>
        /// <returns></returns>
        public Boolean StartGame()
        {
            lock (this)
            {
                if (!IsGameInitialized)
                    return false;

                IsGameInitialized = false;

                // Time span of present + 10 seconds:
                TimeSpan when = DateTime.Now.TimeOfDay + TimeSpan.FromSeconds(10);

                // Send start game message.
                // If the command is commited, then it starts the game:
                SendStartGameMessage(when);

                return true;
            }
        }

        /// <summary>
        /// Update current instruction elapsed time.
        /// </summary>
        /// <param name="deltaMilliseconds"></param>
        public void Update(Int32 deltaMilliseconds)
        {
            lock (this)
            {
                if (!IsGameRunning)
                    return;

                InstructionElapsedTime += deltaMilliseconds;
                if (InstructionElapsedTime > InstructionTimeout)
                {
                    InstructionElapsedTime -= InstructionTimeout;

                    HandleInstructionTimeout();
                }
            }
        }

        /// <summary>
        /// Called when a button is clicked.
        /// </summary>
        /// <param name="name"></param>
        public void ButtonClicked(String name)
        {
            lock (this)
            {
                SendButtonClickMessage(name);

                // Handle button clicked event locally:
                HandleButtonClicked(name);
            }
        }

        private void HandleButtonClicked(String name)
        {
            if (CurrentInstruction == name)
            {
                // Request as a client to increment right moves:
                SendRightMoveMessage();

                lock (_lockObject)
                {
                    // Reset timeout:
                    InstructionElapsedTime = 0;
                }

                // Generate a new instruction:
                GenerateNewInstruction();
            }
        }

        private void HandleInstructionTimeout()
        {
            // Request as a client to increment wrong moves:
            SendWrongMoveMessage();

            // Generate a new instruction:
            GenerateNewInstruction();
        }

        private void GenerateNewInstruction()
        {
            // Select player which will have to perform instruction:
            Int32 nextPlayerIndex = Random.Next(0, CurrentPlayersButtons.Count);

            // Get player instructions:
            var nextPlayerInstructions = CurrentPlayersButtons.ElementAt(nextPlayerIndex).Value;

            // Select instruction from corresponding player:
            Int32 nextInstructionIndex = Random.Next(0, nextPlayerInstructions.Count);
            String nextInstruction = nextPlayerInstructions.ElementAt(nextInstructionIndex);

            // Set current instruction:
            CurrentInstruction = nextInstruction;

            // Notify view that instruction has changed:
            if (InstructionChanged != null)
                InstructionChanged(this, new InstructionsChangedEventArgs(nextInstruction));
        }

        private Dictionary<String, HashSet<String>> GenerateAllButtons(List<String> otherPlayers, Int32 numberButtonsPerPlayer)
        {
            Int32 numberPlayers = otherPlayers.Count + 1;
            Dictionary<String, HashSet<String>> result = new Dictionary<String, HashSet<String>>(numberPlayers);

            HashSet<String> assignedButtons = new HashSet<String>();
            foreach (var playerID in otherPlayers)
            {
                var buttonsSet = GenerateButtonsSet(numberButtonsPerPlayer, assignedButtons);
                result.Add(playerID, buttonsSet);
            }

            var myButtonsSet = GenerateButtonsSet(numberButtonsPerPlayer, assignedButtons);
            result.Add(Network.ID, myButtonsSet);

            return result;
        }

        private HashSet<String> GenerateButtonsSet(Int32 numberButtons, HashSet<String> assignedButtons)
        {
            HashSet<String> buttonsSet = new HashSet<String>();

            while (buttonsSet.Count != numberButtons)
            {
                String button = GenerateRandomButton();

                if (!assignedButtons.Contains(button))
                {
                    assignedButtons.Add(button);
                    buttonsSet.Add(button);
                }
            }

            return buttonsSet;
        }

        private String GenerateRandomButton()
        {
            String s = AllInstructions.ElementAt(Random.Next(AllInstructions.Count));
            return s;
        }

        private void SendInitializeGameMessages(Int32 numberRightMoves, Int32 numberWrongMoves)
        {
            var servers = Network.GetServers();

            var playersButtons = GenerateAllButtons(servers, NumberButtons);

            var message = new InitializeGameMessage(
                numberRightMoves, numberWrongMoves, playersButtons
                );

            Raft.SendClientRequest(message.Serialize());
        }

        private void SendJoinLateGameMessage(HashSet<String> buttons)
        {
            JoinLateGameMessage message = new JoinLateGameMessage(Network.ID, buttons);

            Raft.SendClientRequest(message.Serialize());
        }

        private void SendStartGameMessage(TimeSpan when)
        {
            StartGameMessage message = new StartGameMessage(when);

            Raft.SendClientRequest(message.Serialize());
        }

        private void SendButtonClickMessage(String button)
        {
            // Create a message to hold the button name:
            ButtonClickedMessage message = new ButtonClickedMessage(button);

            // Advertise that a button was clicked:
            Network.SendAll(message.Serialize());
        }

        private void SendRightMoveMessage()
        {
            RightMoveMessage message = new RightMoveMessage();

            Raft.SendClientRequest(message.Serialize());
        }

        private void SendWrongMoveMessage()
        {
            WrongMoveMessage message = new WrongMoveMessage();

            Raft.SendClientRequest(message.Serialize());
        }

        private void Network_ServerDisconnected(object sender, ServerEventArgs e)
        {
            lock (this)
            {
                // Remove player buttons if they exist:
                CurrentPlayersButtons.Remove(e.ServerID);
            }
        }

        private void Network_DataReceived(object sender, DataReceivedEventArgs e)
        {
            String data = e.Data;

            // Ignore mal formed messages:
            String type;
            if (!Message.FindType(data, out type))
                return;

            lock (this)
            {
                // Handle button clicked message:
                if (type == ButtonClickedMessage.StaticType)
                    HandleButtonClickedMessage(ButtonClickedMessage.Deserialize(data));
            }
        }

        private void HandleButtonClickedMessage(ButtonClickedMessage message)
        {
            // Handle button clicked event locally:
            HandleButtonClicked(message.Name);
        }

        private void Raft_MachineStateChanged(object sender, PersistentState.MachineStateChangedEventArgs e)
        {
            String data = e.Command;

            // Ignore mal formed messages:
            String type;
            if (!Message.FindType(data, out type))
                return;

            lock (this)
            {
                // Handle initialize game message:
                if (type == InitializeGameMessage.StaticType)
                    HandleInitializeGameMessage(InitializeGameMessage.Deserialize(data));

                // Handle join late game message:
                else if (type == JoinLateGameMessage.StaticType)
                    HandleJoinLateGameMessage(JoinLateGameMessage.Deserialize(data));

                // Handle start game message:
                else if (type == StartGameMessage.StaticType)
                    HandleStartGameMessage(StartGameMessage.Deserialize(data));

                // Handle right move message:
                else if (type == RightMoveMessage.StaticType)
                    HandleRightMovesMessage();

                // Handle wrong move message:
                else if (type == WrongMoveMessage.StaticType)
                    HandleWrongMovesMessage();
            }
        }

        private void HandleInitializeGameMessage(InitializeGameMessage message)
        {
            // Initialize game state:
            MaxRightMoves = message.NumberRightMoves;
            MaxWrongMoves = message.NumberWrongMoves;
            RightMoves = 0;
            WrongMoves = 0;
            InstructionTimeout = 5000;
            InstructionElapsedTime = 0;

            // Initialize screen:
            CurrentPlayersButtons = message.Buttons;

            // If player is not in the current game:
            if (!CurrentPlayersButtons.ContainsKey(Network.ID))
            {
                // Find out buttons already assigned:
                HashSet<String> assignedButtons = new HashSet<String>();
                foreach (HashSet<String> playerButtons in CurrentPlayersButtons.Values)
                    foreach (String button in playerButtons)
                        assignedButtons.Add(button);

                // Assign buttons:
                HashSet<String> buttons = GenerateButtonsSet(NumberButtons, assignedButtons);

                // Send join late game message:
                SendJoinLateGameMessage(buttons);

                return;
            }

            CurrentButtons = CurrentPlayersButtons[Network.ID];

            IsGameInitialized = true;

            // Notify that the game was initialized:
            if (GameInitialized != null)
                GameInitialized(this, new GameInitializedEventArgs(CurrentButtons));
        }

        private void HandleJoinLateGameMessage(JoinLateGameMessage message)
        {
            String serverID = message.ServerID;
            HashSet<String> buttons = message.Buttons;

            lock (this)
            {
                if (serverID == Network.ID)
                {
                    CurrentButtons = buttons;

                    IsGameInitialized = true;

                    // Notify that the game was initialized:
                    if (GameInitialized != null)
                        GameInitialized(this, new GameInitializedEventArgs(CurrentButtons));

                    IsGameRunning = true;

                    // Generate first instruction:
                    GenerateNewInstruction();
                }

                // Add new player instructions:
                if (CurrentPlayersButtons.ContainsKey(serverID))
                    CurrentPlayersButtons[serverID] = buttons;
                else
                    CurrentPlayersButtons.Add(serverID, buttons);
            }
        }

        private void HandleStartGameMessage(StartGameMessage message)
        {            
            StartTime = message.When;

            lock (Scheduler)
            {
                Scheduler.RunAtSpecificTime(
                    message.When,
                    function: () =>
                    {
                        // Raise event to start game on view:
                        GameStarted(this, EventArgs.Empty);

                        // Generate first instruction:
                        GenerateNewInstruction();

                        IsGameRunning = true;
                    });
            }
        }

        private void HandleRightMovesMessage()
        {
            RightMoves++;

            if (MaxRightMoves <= RightMoves)
            {
                NotifyGameEnded(won: true);
            }
        }

        private void HandleWrongMovesMessage()
        {
            WrongMoves++;

            if (MaxWrongMoves <= WrongMoves)
            {
                NotifyGameEnded(won: false);
            }
        }

        private void NotifyGameEnded(Boolean won)
        {
            IsGameRunning = false;

            if (GameEnded != null)
                GameEnded(this, new GameEndedEventArgs(won));
        }
    }
}
