﻿using System;
using System.Timers;

namespace Assets.Scripts.Model.Logic
{
    public class Scheduler
    {
        private Timer Timer { get; set; }
        private Action FunctionToCall { get; set; }

        public Scheduler()
        {
            Timer = new Timer();
            Timer.Elapsed += Timer_Elapsed;
        }

        public void RunAtSpecificTime(TimeSpan when, Action function)
        {
            DateTime currentTime = DateTime.Now;
            TimeSpan timeToGo = when - currentTime.TimeOfDay;

            Int32 interval = 0;
            if (timeToGo > TimeSpan.Zero)
                interval = Convert.ToInt32(timeToGo.TotalMilliseconds);

            FunctionToCall = function;
            Timer.Interval = interval;
            Timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            FunctionToCall();

            Timer.Stop();
        }
    }
}
