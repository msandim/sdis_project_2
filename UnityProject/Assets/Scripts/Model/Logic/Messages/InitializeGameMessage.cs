﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Model.Logic.Messages
{
    using Net.Messages;

    public class InitializeGameMessage : Message
    {
        public static readonly String StaticType = "InitializeGameMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public Int32 NumberRightMoves { get; private set; }
        public Int32 NumberWrongMoves { get; private set; }
        public Dictionary<String, HashSet<String>> Buttons { get; private set; }

        public InitializeGameMessage(Int32 numberRightMoves, Int32 numberWrongMoves, Dictionary<String, HashSet<String>> buttons)
        {
            NumberRightMoves = numberRightMoves;
            NumberWrongMoves = numberWrongMoves;
            Buttons = buttons;
        }

        public static InitializeGameMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<InitializeGameMessage>(jsonValue);
        }
    }
}
