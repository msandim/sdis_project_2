﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Logic.Messages
{
    using Net.Messages;

    public class StartGameMessage : Message
    {
        public static readonly String StaticType = "StartGameMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public TimeSpan When { get; private set; }

        public StartGameMessage(TimeSpan when)
        {
            When = when;
        }

        public static StartGameMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<StartGameMessage>(jsonValue);
        }
    }
}
