﻿using System;

namespace Assets.Scripts.Model.Logic.Messages
{
    using Net.Messages;

    public class WrongMoveMessage : Message
    {
        public static readonly String StaticType = "WrongMoveMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }
    }
}
