﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Logic.Messages
{
    using Net.Messages;

    public class ButtonClickedMessage : Message
    {
        public static readonly String StaticType = "ButtonClickedMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public String Name { get; private set; }

        public ButtonClickedMessage(String name)
        {
            Name = name;
        }

        public static ButtonClickedMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<ButtonClickedMessage>(jsonValue);
        }
    }
}