﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Model.Logic.Messages
{
    using Net.Messages;

    public class JoinLateGameMessage : Message
    {
        public static readonly String StaticType = "JoinLateGame";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public String ServerID { get; private set; }
        public HashSet<String> Buttons { get; private set; }

        public JoinLateGameMessage(String serverID, HashSet<String> buttons)
        {
            ServerID = serverID;
            Buttons = buttons;
        }

        public static JoinLateGameMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<JoinLateGameMessage>(jsonValue);
        }
    }
}
