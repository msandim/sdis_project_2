﻿using System;

namespace Assets.Scripts.Model.Logic.Messages
{
    using Net.Messages;

    public class RightMoveMessage : Message
    {
        public static readonly String StaticType = "RightMoveMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }
    }
}
