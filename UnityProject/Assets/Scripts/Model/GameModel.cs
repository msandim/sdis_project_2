﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Model
{
    using Logic;
    using Net;
    using Net.Sockets;
    using Raft;
    using Raft.State;

    public class GameModel
    {
        public static readonly String DefaultInstructionsFilename = @"..\..\Resources\InstructionsDictionary.txt";

        public event EventHandler<PersistentState.LeaderChangedEventArgs> RaftLeaderChanged;
        public event EventHandler<PersistentState.MachineStateChangedEventArgs> MachineStateChanged;
        public event EventHandler<RaftServer.StateChangedEventArgs> RaftStateChanged;
        public event EventHandler<ServerEventArgs> PlayerConnected;
        public event EventHandler<ServerEventArgs> PlayerDisconnected;
        public event EventHandler<DataReceivedEventArgs> DataReceived;
        public event EventHandler<GameLogic.GameInitializedEventArgs> GameInitialized;
        public event EventHandler GameStarted;
        public event EventHandler<GameLogic.GameEndedEventArgs> GameEnded;
        public event EventHandler<GameLogic.InstructionsChangedEventArgs> InstructionChanged;

        public NetworkStatus NetworkStatus
        {
            get
            {
                return Network.Status;
            }
        }
        public String NetworkAddress
        {
            get
            {
                return Network.Address;
            }
        }
        public Int32 NetworkPort
        {
            get
            {
                return Network.Port;
            }
        }
        public List<Connection> NetworkConnections
        {
            get
            {
                return Network.GetConnections();
            }
        }
        public AbstractState.Type RaftStatus
        {
            get
            {
                return RaftServer.Status;
            }
        }
        public HashSet<String> CurrentButtons
        {
            get
            {
                lock(Logic)
                {
                    return Logic.CurrentButtons;
                }
            }
        }
        public Boolean IsGameRunning
        {
            get
            {
                return Logic.IsGameRunning;
            }
        }
        public TimeSpan GameStartTime
        {
            get
            {
                return Logic.StartTime;
            }
        }
        public Int32 InstructionElapsedTime
        {
            get
            {
                return Logic.InstructionElapsedTime;
            }
        }
        public Int32 InstructionTimeout
        {
            get
            {
                return Logic.InstructionTimeout;
            }
        }
        public String CurrentInstruction
        {
            get
            {
                return Logic.CurrentInstruction;
            }
        }
        public Boolean IsHost { get; set; }

        private INetwork Network { get; set; }
        private RaftServer RaftServer { get; set; }
        private GameLogic Logic { get; set; } // TODO generate buttons, create events for win, game over, right and wrong moves 

        public GameModel()
        {
            Network = new NetworkManager();
            RaftServer = new RaftServer();
            Logic = new GameLogic();

            Network.DataReceived += Network_DataReceived;
            Network.ServerConnected += Network_ServerConnected;
            Network.ServerDisconnected += Network_ServerDisconnected;
            RaftServer.LeaderChanged += RaftServer_LeaderChanged;
            RaftServer.MachineStateChanged += RaftServer_MachineStateChanged;
            RaftServer.StateChanged += RaftServer_StateChanged;
            Logic.InstructionChanged += Logic_InstructionChanged;
            Logic.GameInitialized += Logic_GameInitialized;
            Logic.GameStarted += Logic_GameStarted;
            Logic.GameEnded += Logic_GameEnded;
        }

        public void Initialize(String address, Int32 port, String instructionsFilename)
        {
            Network.Initialize(address, port);
            RaftServer.Initialize(Network);
            Logic.Initialize(instructionsFilename, RaftServer, Network);
        }

        public void Shutdown()
        {
            Logic.Shutdown();
            RaftServer.Shutdown();
            Network.Shutdown();
        }

        public void JoinRoom(String remoteAddress, Int32 remotePort)
        {
            Network.ConnectTo(remoteAddress, remotePort);
        }

        public void ExitRoom()
        {
            Network.ClearConnections();
        }

        public void ClearState()
        {
            RaftServer.ClearState();
        }

        public void InitializeGame()
        {
            Logic.InitializeGame();
        }

        public void StartGame()
        {
            Logic.StartGame();
        }

        public void ButtonClicked(String name)
        {
            Logic.ButtonClicked(name);
        }

        public void Update(Int32 deltaMilliseconds)
        {
            Logic.Update(deltaMilliseconds);
        }

        private void Network_ServerConnected(object sender, ServerEventArgs e)
        {
            if (PlayerConnected != null)
                PlayerConnected(this, e);
        }

        private void Network_ServerDisconnected(object sender, ServerEventArgs e)
        {
            if (PlayerDisconnected != null)
                PlayerDisconnected(this, e);
        }

        private void Network_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (DataReceived != null)
                DataReceived(this, e);
        }

        private void RaftServer_LeaderChanged(object sender, PersistentState.LeaderChangedEventArgs e)
        {
            if (RaftLeaderChanged != null)
                RaftLeaderChanged(this, e);
        }

        private void RaftServer_MachineStateChanged(object sender, PersistentState.MachineStateChangedEventArgs e)
        {
            if (MachineStateChanged != null)
                MachineStateChanged(this, e);
        }

        private void RaftServer_StateChanged(object sender, RaftServer.StateChangedEventArgs e)
        {
            if (RaftStateChanged != null)
                RaftStateChanged(this, e);
        }

        private void Logic_InstructionChanged(object sender, GameLogic.InstructionsChangedEventArgs e)
        {
            if (InstructionChanged != null)
                InstructionChanged(this, e);
        }

        private void Logic_GameInitialized(object sender, GameLogic.GameInitializedEventArgs e)
        {
            if (GameInitialized != null)
                GameInitialized(this, e);
        }

        private void Logic_GameStarted(object sender, EventArgs e)
        {
            if (GameStarted != null)
                GameStarted(this, e);
        }

        private void Logic_GameEnded(object sender, GameLogic.GameEndedEventArgs e)
        {
            if (GameEnded != null)
                GameEnded(this, e);
        }
    }
}
