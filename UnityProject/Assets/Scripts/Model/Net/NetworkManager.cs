﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Assets.Scripts.Model.Net.Sockets;

namespace Assets.Scripts.Model.Net
{
    using Messages;

    public class NetworkManager : INetwork
    {
        private object _dataReceivedLock = new object();

        private event EventHandler<DataReceivedEventArgs> _dataReceived;
        public event EventHandler<DataReceivedEventArgs> DataReceived
        {
            add
            {
                lock(_dataReceivedLock)
                {
                    _dataReceived += value;
                }
            }
            remove
            {
                lock(_dataReceivedLock)
                {
                    _dataReceived -= value;
                }
            }
        }
        public event EventHandler<ServerEventArgs> ServerConnected;
        public event EventHandler<ServerEventArgs> ServerDisconnected;

        public String ID { get; private set; }
        public String Address { get; private set; }
        public Int32 Port { get; private set; }
        public Int32 NumberServers
        {
            get
            {
                lock(Servers)
                {
                    return Servers.Count;
                }
            }
        } 
        public NetworkStatus Status
        {
            get
            {
                if (NumberServers == 0)
                {
                    if (SocketsManager.IsListening)
                        return NetworkStatus.Listening;

                    return NetworkStatus.Disconnected;
                }

                return NetworkStatus.Connected;
            }
        }
        private Dictionary<String, Connection> Servers { get; set; }
        private TcpSocketsManager SocketsManager { get; set; }

        public NetworkManager()
        {
            Servers = new Dictionary<String, Connection>();
            SocketsManager = new TcpSocketsManager();
        }

        public void Initialize(String address, Int32 port)
        {
            ID = GenerateID(address, port);
            Address = address;
            Port = port;

            lock(Servers)
            {
                Servers.Clear();
            }

            lock(SocketsManager)
            {
                SocketsManager.SocketDisconnected += SocketsManager_SocketDisconnected;
                SocketsManager.DataReceived += SocketsManager_DataReceived;
                SocketsManager.Initialize(address, port);
            }
        }

        public void Shutdown()
        {
            lock(SocketsManager)
            {
                SocketsManager.Shutdown();
            }

            lock(Servers)
            {
                Servers.Clear();
            }
        }

        public Boolean ConnectTo(String remoteAddress, Int32 remotePort)
        {
            if (!ConnectTo(remoteAddress, remotePort, true))
                return false;

            return true;
        }

        public void ClearConnections()
        {
            SocketsManager.Shutdown();
            SocketsManager.Initialize(Address, Port);
        }

        private Boolean ConnectTo(String remoteAddress, Int32 remotePort, Boolean requestNetworkInformation)
        {
            Connection connection = new Connection(remoteAddress, remotePort);

            lock (Servers)
            {
                // If server is already connected:
                if (Servers.ContainsKey(connection.ID))
                    return true;

                // If socket creation failed:
                if (!SocketsManager.CreateSocket(connection))
                    return false;

                // Add server to connected servers:
                Servers.Add(connection.ID, connection);
            }

            // Send a connect message:
            SendConnectMessage(connection.ID, requestNetworkInformation);

            return true;
        }

        public Boolean Send(String serverID, String data)
        {
            Connection connection;

            lock (Servers)
            {
                if (!Servers.ContainsKey(serverID))
                    return false;

                // Get connection corresponding to the serverID:
                connection = Servers[serverID];
            }

            // Create a transport message to identify this server:
            TransportMessage message = new TransportMessage(ID, data);

            // Send transport message:
            SocketsManager.Send(connection, message.Serialize());

            return true;
        }

        public void SendAll(String data)
        {
            // Create a transport message to identify this server:
            TransportMessage message = new TransportMessage(ID, data);

            // Send message through all connected sockets:
            SocketsManager.SendAll(message.Serialize());
        }

        private void SendConnectMessage(String serverID, Boolean requestNetworkInformation)
        {
            // Create a connect message:
            ConnectMessage message = new ConnectMessage(requestNetworkInformation);

            // Send connect message:
            Send(serverID, message.Serialize());
        }

        private void SendNetworkInformationMessage(String serverID)
        {
            List<String> connectedServerIDs;

            lock (Servers)
            {
                // Convert values from dictionary to a list:
                connectedServerIDs = Servers.Keys.ToList<String>();
            }

            // Remove target connected server:
            connectedServerIDs.Remove(serverID);

            // Create a network information message:
            NetworkInformationMessage networkInformationMessage = new NetworkInformationMessage(connectedServerIDs);

            // Send message to the specified server:
            Send(serverID, networkInformationMessage.Serialize());
        }

        private void SocketsManager_DataReceived(object sender, TcpSocketsManager.DataReceivedEventArgs e)
        {
            String data = e.Data;
            Connection connection = e.Connection;

            // Ignore mal formed messages:
            String type;
            if (!Message.FindType(data, out type))
                return;

            // Ignore other messages:
            if (type != TransportMessage.StaticType)
                return;

            // Create a transport message:
            TransportMessage message = TransportMessage.Deserialize(data);

            String serverID = message.ServerID;
            String transportedData = message.Data;

            // Ignore mal formed messages:
            String transportedDataType;
            if (!Message.FindType(transportedData, out transportedDataType))
                return;

            // Handle connect message:
            if (transportedDataType == ConnectMessage.StaticType)
                HandleConnectMessage(serverID, connection, ConnectMessage.Deserialize(transportedData));

            // Handle network information message:
            else if (transportedDataType == NetworkInformationMessage.StaticType)
                HandleNetworkInformationMessage(NetworkInformationMessage.Deserialize(transportedData));

            // Handle other messages:
            else if (_dataReceived != null)
                _dataReceived(this, new DataReceivedEventArgs(serverID, transportedData));
        }

        private void HandleConnectMessage(String serverID, Connection connection, ConnectMessage message)
        {
            lock (Servers)
            {
                // Ignore if server is in the connected servers:
                if (Servers.ContainsKey(serverID))
                    return;

                // Add connection to connected servers:
                Servers.Add(serverID, connection);
            }

            // Send Network Information Message:
            SendNetworkInformationMessage(serverID);

            // Notify that a server was connected:
            if (ServerConnected != null)
                ServerConnected(this, new ServerEventArgs(serverID));
        }

        private void HandleNetworkInformationMessage(NetworkInformationMessage message)
        {
            // Connect with all other peers:
            foreach (var serverID in message.ConnectedServers)
            {
                Connection connection;
                if (!Connection.TryParse(serverID, out connection))
                    continue;

                ConnectTo(
                    connection.Address,
                    connection.Port,
                    requestNetworkInformation: false
                    );
            }
        }

        private void SocketsManager_SocketDisconnected(object sender, TcpSocketsManager.SocketDisconnectedEventArgs e)
        {
            Connection connection = e.Connection;
            String disconnectedServerID = null;

            lock(Servers)
            {
                // Remove connection:
                foreach (var pair in Servers)
                {
                    if (pair.Value == connection)
                    {
                        disconnectedServerID = pair.Key;
                        Servers.Remove(disconnectedServerID);

                        break;
                    }
                }
            }
            
            // If no server was found:
            if (disconnectedServerID == null)
                return;

            // Notify that a server disconnected:
            if (ServerDisconnected != null)
                ServerDisconnected(this, new ServerEventArgs(disconnectedServerID));
        }

        private static String GenerateID(String address, Int32 port)
        {
            StringBuilder builder = new StringBuilder(21);
            builder.Append(address);
            builder.Append(":");
            builder.Append(port);

            return builder.ToString();
        }

        public Boolean ContainsServer(string serverID)
        {
            lock(Servers)
            {
                if (Servers.ContainsKey(serverID))
                    return true;

                return false;
            }
        }

        public List<string> GetServers()
        {
            lock(Servers)
            {
                return Servers.Keys.ToList<String>();
            }
        }

        public Boolean AddServer(string serverID, Connection connection)
        {
            lock(Servers)
            {
                if (Servers.ContainsKey(serverID))
                    return false;

                Servers.Add(serverID, connection);
            }

            return true;
        }

        public List<Connection> GetConnections()
        {
            return Servers.Values.ToList<Connection>();
        }
    }
}
