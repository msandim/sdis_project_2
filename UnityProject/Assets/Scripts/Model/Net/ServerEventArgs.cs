﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Net
{
    public class ServerEventArgs : EventArgs
    {
        public String ServerID { get; private set; }

        public ServerEventArgs(String serverID)
        {
            ServerID = serverID;
        }
    }
}
