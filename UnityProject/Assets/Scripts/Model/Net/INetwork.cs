﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Model.Net
{
    using Sockets;

    public interface INetwork
    {
        event EventHandler<DataReceivedEventArgs> DataReceived;
        event EventHandler<ServerEventArgs> ServerConnected;
        event EventHandler<ServerEventArgs> ServerDisconnected;

        String ID { get; }
        String Address { get; }
        Int32 Port { get; }
        Int32 NumberServers { get; }
        NetworkStatus Status { get; }

        void Initialize(String address, Int32 port);
        void Shutdown();
        Boolean ConnectTo(String remoteAddress, Int32 remotePort);
        void ClearConnections();
        Boolean AddServer(String serverID, Connection connection);
        Boolean ContainsServer(String serverID);
        Boolean Send(String serverID, String data);
        void SendAll(String data);
        List<String> GetServers();
        List<Connection> GetConnections();
    }
}
