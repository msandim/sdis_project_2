﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Model.Net.Sockets;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Net
{
    /// <summary>
    /// Used for unit testing.
    /// </summary>
    public class NetworkTester : INetwork
    {
        public class DataSentEventArgs : EventArgs
        {
            public static readonly String ToAll = "ALL";

            public String From { get; private set; }
            public String To { get; private set; }
            public String Data { get; private set; }

            public DataSentEventArgs(String from, String to, String data)
            {
                From = from;
                To = to;
                Data = data;
            }
        }

        public event EventHandler<DataReceivedEventArgs> DataReceived;
        public event EventHandler<DataSentEventArgs> DataSent;
        public event EventHandler<ServerEventArgs> ServerConnected;
        public event EventHandler<ServerEventArgs> ServerDisconnected;

        public String ID { get; private set; }
        public String Address { get; private set; }
        public Int32 Port { get; private set; }
        public Int32 NumberServers
        {
            get
            {
                lock(Servers)
                {
                    return Servers.Count;
                }
            }
        }
        public NetworkStatus Status { get; private set; }
        private Dictionary<String, Connection> Servers { get; set; }

        public NetworkTester()
        {
            Servers = new Dictionary<string, Connection>();
        }

        public void Initialize(String address, Int32 port)
        {
            ID = GenerateID(address, port);
            Address = address;
            Port = port;
        }

        public void Shutdown()
        {
        }

        public void ClearConnections()
        {
        }

        public Boolean ContainsServer(String serverID)
        {
            lock(Servers)
            {
                if (Servers.ContainsKey(serverID))
                    return true;
            }

            return false;
        }

        public Boolean Send(string serverID, string data)
        {
            if (DataSent != null)
                DataSent(this, new DataSentEventArgs(ID, serverID, data));
            else
                Console.WriteLine("NetworkTester:: TestSend delegate not set!");

            return true;
        }

        public void SendAll(string data)
        {
            if (DataSent != null)
                DataSent(this, new DataSentEventArgs(ID, DataSentEventArgs.ToAll, data));
        }

        public void SendTestMessage(String serverID, String data)
        {
            if(DataReceived != null)
                DataReceived(this, new DataReceivedEventArgs(serverID, data));
        }

        public Boolean ConnectTo(string remoteAddress, int remotePort)
        {
            throw new NotImplementedException();
        }

        public List<String> GetServers()
        {
            lock(Servers)
            {
                return Servers.Keys.ToList<String>();
            }
        }

        private static String GenerateID(String address, Int32 port)
        {
            StringBuilder builder = new StringBuilder(21);
            builder.Append(address);
            builder.Append(":");
            builder.Append(port);

            return builder.ToString();
        }

        public Boolean AddServer(String serverID, Connection connection)
        {
            lock(Servers)
            {
                if (Servers.ContainsKey(serverID))
                    return false;

                Servers.Add(serverID, connection);
            }

            return true;
        }

        public List<Connection> GetConnections()
        {
            return Servers.Values.ToList<Connection>();
        }
    }
}
