﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Net.Messages
{
    public class TestMessage : Message
    {
        public static readonly String StaticType = "TestMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public String ServerID { get; private set; }
        public Int32 MessageID { get; private set; }

        public TestMessage(String serverID, Int32 messageID)
        {
            ServerID = serverID;
            MessageID = messageID;
        }

        public static TestMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<TestMessage>(jsonValue);
        }
    }
}
