﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Net.Messages
{
    public class ConnectMessage : Message
    {
        public static readonly String StaticType = "ConnectMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public Boolean RequestNetworkInformation { get; private set; }

        public ConnectMessage(Boolean requestNetworkInformation)
        {
            RequestNetworkInformation = requestNetworkInformation;
        }

        public static ConnectMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<ConnectMessage>(jsonValue);
        }
    }
}
