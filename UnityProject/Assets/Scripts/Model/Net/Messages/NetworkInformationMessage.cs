﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Net.Messages
{
    using Sockets;

    public class NetworkInformationMessage : Message
    {
        public static readonly String StaticType = "NetworkInformationMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public List<String> ConnectedServers { get; private set; }

        public NetworkInformationMessage(List<String> connectedServers)
        {
            ConnectedServers = connectedServers;
        }

        public static NetworkInformationMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<NetworkInformationMessage>(jsonValue);
        }
    }
}
