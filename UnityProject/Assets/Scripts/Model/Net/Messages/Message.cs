﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;

namespace Assets.Scripts.Model.Net.Messages
{
    public abstract class Message : EventArgs
    {
        public abstract String Type { get; }

        public static Boolean FindType(String messageJson, out String type)
        {
            try
            {
                JObject message = JObject.Parse(messageJson);
                type = message.Value<String>("Type");
            }
            catch
            {
                type = null;
                return false;
            }

            return true;
        }

        public String Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
