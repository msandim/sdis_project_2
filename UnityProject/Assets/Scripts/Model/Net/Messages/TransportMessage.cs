﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Net.Messages
{
    public class TransportMessage : Message
    {
        public static readonly String StaticType = "TransportMessage";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public String ServerID { get; private set; }
        public String Data { get; private set; }

        public TransportMessage(String serverID, String data)
        {
            ServerID = serverID;
            Data = data;
        }

        public static TransportMessage Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<TransportMessage>(jsonValue);
        }
    }
}
