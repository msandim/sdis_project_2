﻿using System;

namespace Assets.Scripts.Model.Net
{
    public class DataReceivedEventArgs : EventArgs
    {
        public String ServerID { get; private set; }
        public String Data { get; private set; }

        public DataReceivedEventArgs(String serverID, String data)
        {
            ServerID = serverID;
            Data = data;
        }
    }
}
