﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Net
{
    public enum NetworkStatus
    {
        Disconnected, Listening, Connecting, Connected
    }
}
