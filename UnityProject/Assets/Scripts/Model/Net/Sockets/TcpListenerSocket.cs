﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Assets.Scripts.Model.Net.Sockets
{
    using Messages;

    public class TcpListenerSocket
    {
        public class SocketConnectedEventArgs
        {
            public Socket SocketHandler { get; private set; }
            public String Address { get; private set; }
            public Int32 Port { get; private set; }


            public SocketConnectedEventArgs(Socket socketHandler)
            {
                SocketHandler = socketHandler;

                // Retrieve address and port:
                IPEndPoint remoteEndPoint = (IPEndPoint) socketHandler.RemoteEndPoint;
                Address = remoteEndPoint.Address.ToString();
                Port = remoteEndPoint.Port;
            }
        }

        public delegate void SocketConnectedEventHandler(TcpListenerSocket sender, SocketConnectedEventArgs e);
        public event SocketConnectedEventHandler SocketConnected;

        
        public Boolean IsListening
        {
            get
            {
                return _isListening;
            }
        }
        protected Socket SocketHandler { get; set; }
        private Thread ListeningThread { get; set; }
        private ManualResetEvent AcceptWaitHandle { get; set; }
        private volatile Boolean _isListening;

        public TcpListenerSocket()
        {
            AcceptWaitHandle = new ManualResetEvent(false);
        }

        public void Initialize(String address, Int32 port)
        {
            // Create a TCP socket:
            SocketHandler = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse(address), port);
            SocketHandler.Bind(localEndPoint);
            SocketHandler.Listen(backlog: 100);
        }

        public void Shutdown()
        {
            // Disable sending and receiveiving operations on this socket:
            if (SocketHandler.Connected)
                SocketHandler.Shutdown(SocketShutdown.Both);

            // Close the socket and release associated resources:
            SocketHandler.Close();
        }

        public void StartListening()
        {
            _isListening = true;
            ListeningThread = new Thread(DoListening);
            ListeningThread.Start();
        }

        private void DoListening()
        {
            while (_isListening)
            {
                // Set state to nonsignaled:
                AcceptWaitHandle.Reset();

                // Begin an asynchronous operation to accept an incoming connection attempt:
                SocketHandler.BeginAccept(AcceptSocketCallback, SocketHandler);

                // Block until signal from AcceptSocketCallback:
                AcceptWaitHandle.WaitOne();
            }
        }

        private void AcceptSocketCallback(IAsyncResult result)
        {
            // The waiting thread in the Start function can proceed:
            AcceptWaitHandle.Set();

            // Asynchronously accept an incoming connection attempt:
            try
            {
                // Asynchronously accept an incoming connection attempt: 
                Socket socketHandler = SocketHandler.EndAccept(result);

                // Notify that a socket was connected:
                if (SocketConnected != null)
                    SocketConnected(this, new SocketConnectedEventArgs(socketHandler));
            }
            catch (ObjectDisposedException)
            {
            }
        }

        public void StopListening()
        {
            // _isListening is a volatile boolean, so all operations are atomic:
            _isListening = false;

            // The waiting thread in the Start function can proceed:
            AcceptWaitHandle.Set();

            // Wait for listening thread to terminate:
            ListeningThread.Join();
        }
    }
}