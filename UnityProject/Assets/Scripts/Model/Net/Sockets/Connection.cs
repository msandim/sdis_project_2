﻿using System;
using System.Net;
using System.Text;

namespace Assets.Scripts.Model.Net.Sockets
{
    public class Connection
    {
        public String ID { get; private set; }
        private Int32 HashCode { get; set; }
        public String Address { get; private set; }
        public Int32 Port { get; private set; }

        public Connection(String address, Int32 port)
        {
            ID = GenerateConnectionID(address, port);
            HashCode = ID.GetHashCode();
            Address = address;
            Port = port;
        }

        public override Boolean Equals(object obj)
        {
            if (obj == null || !(obj is Connection))
                return false;

            Connection connection = (Connection)obj;
            if (!ID.Equals(connection.ID))
                return false;

            return true;
        }

        public Boolean Equals(Connection connection)
        {
            if (connection == null)
                return false;

            if (!ID.Equals(connection.ID))
                return false;

            return true;
        }

        public override Int32 GetHashCode()
        {
            return HashCode;
        }

        public static Boolean TryParse(String connectionID, out Connection connection)
        {
            connection = null;

            String[] split = connectionID.Split(':');
            if (split.Length != 2)
                return false;

            IPAddress ipAddress;
            if (!IPAddress.TryParse(split[0], out ipAddress))
                return false;

            Int32 port;
            if (!Int32.TryParse(split[1], out port))
                return false;

            connection = new Connection(ipAddress.ToString(), port);

            return true;
        }

        public static String GenerateConnectionID(String address, Int32 port)
        {
            StringBuilder builder = new StringBuilder(capacity: 21);
            builder.Append(address);
            builder.Append(":");
            builder.Append(port);

            return builder.ToString();
        }
    }
}
