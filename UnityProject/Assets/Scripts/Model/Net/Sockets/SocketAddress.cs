﻿using System;
using System.Linq;
using System.Threading;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Collections.Generic;

namespace Assets.Scripts.Model.Net.Sockets
{
    public static class SocketAddress
    {
        private static Int32 _basePort = 25001;
        private static object _unusedPortMutex = new object();


        private static Int32 _unusedPort;
        public static Int32 UnusedPort
        {
            get
            {
                Int32 output;

                lock (_unusedPortMutex)
                {
                    output = _unusedPort++;
                }

                return output;
            }
        }

        private static String _localIP;
        public static String LocalIP
        {
            get
            {
                return _localIP;
            }
        }

        public static List<String> LocalIPs
        {
            get
            {
                List<String> output = new List<string>();

                var addresses = Dns.GetHostAddresses("");
                foreach (var address in addresses)
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                        output.Add(address.ToString());

                return output;           
            }
        }

        static SocketAddress()
        {
            _unusedPort = _basePort;
            _localIP = "127.0.0.1";
        }
    }
}
