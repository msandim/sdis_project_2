﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Assets.Scripts.Model.Net.Sockets
{
    using Messages;

    public class TcpSenderSocket
    {
        public class StateChangedEventArgs : EventArgs
        {
            public StateObject State { get; set; }

            public StateChangedEventArgs(StateObject state)
            {
                State = state;
            }
        }
        public class StateObject
        {
            public const int BufferSize = 1024;

            public Connection Connection { get; private set; }
            public Socket Socket { get; private set; }
            public Byte[] Buffer { get; private set; }
            public String Data { get; set; }

            public StateObject(Connection connection, Socket socket)
            {
                Connection = connection;
                Socket = socket;
                Buffer = new Byte[BufferSize];
                Data = String.Empty;
            }
        }

        public delegate void StateChangedEventHandler(TcpSenderSocket sender, StateChangedEventArgs e);
        public delegate void DisconnectedEventHandler(TcpSenderSocket sender, EventArgs e);

        public event StateChangedEventHandler DataReceived;
        public event StateChangedEventHandler DataSent;
        public event DisconnectedEventHandler Disconnected;

        public Connection Connection { get; private set; }
        private ManualResetEvent ConnectWaitHandle { get; set; }
        private ManualResetEvent ReceiveWaitHandle { get; set; }
        private ManualResetEvent SendWaitHandle { get; set; }
        private Socket SocketHandler { get; set; }
        private Thread ListeningThread { get; set; }
        private Boolean IsConnected
        {
            get
            {
                try
                {
                    lock(SocketHandler)
                    {
                        // Is false if connection is closed, terminated:
                        bool part1 = SocketHandler.Poll(1000, SelectMode.SelectRead);

                        // Is false if there is no bytes available to read:
                        bool part2 = (SocketHandler.Available == 0);

                        // If both are false, then there is no connection:
                        if (part1 && part2)
                            return false;
                    }

                    return true;
                }
                catch
                {
                    return false;   
                }
            }
        }
        private volatile Boolean _connected;
        private volatile Boolean _listening;

        public TcpSenderSocket()
        {
            ConnectWaitHandle = new ManualResetEvent(false);
            ReceiveWaitHandle = new ManualResetEvent(false);
            SendWaitHandle = new ManualResetEvent(false);
            ListeningThread = new Thread(DoListening);
        }

        public Boolean Initialize(Socket socketHandler)
        {
            IPEndPoint remoteEndPoint = (IPEndPoint)socketHandler.RemoteEndPoint;
            Connection = new Connection(remoteEndPoint.Address.ToString(), remoteEndPoint.Port);

            // Check if socket is valid:
            if (!socketHandler.Connected)
                return false;

            // Set socket handler:
            SocketHandler = socketHandler;

            return true;
        }

        public Boolean Initialize(Connection connection)
        {
            Connection = connection;

            // Create a TCP socket:
            SocketHandler = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Create an IPEndPoint object with the given address and port:
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse(connection.Address), connection.Port);

            // Begin an asynchronous request for a remote host connection:
            SocketHandler.BeginConnect(remoteEndPoint, ConnectCallback, SocketHandler);

            // Block until signal from ConnectCallback:
            ConnectWaitHandle.WaitOne();

            // Check if connection was successful:
            if (!_connected)
                return false;

            return true;
        }

        private void ConnectCallback(IAsyncResult result)
        {
            Socket socketHandler = (Socket)result.AsyncState;

            if (!socketHandler.Connected)
            {
                // Signal main thread that connection was not successful:
                _connected = false;

                // Signal main thread to continue:
                ConnectWaitHandle.Set();

                return;
            }

            // End the asynchronous connection request:
            socketHandler.EndConnect(result);

            // Signal main thread that connection was successful:
            _connected = true;

            // Signal main thread to continue:
            ConnectWaitHandle.Set();
        }

        public void Shutdown()
        {
            if (_listening)
                StopListening();

            // Disable sending and receiveiving operations on this socket:
            if (SocketHandler.Connected)
                SocketHandler.Shutdown(SocketShutdown.Both);

            // Close the socket and release associated resources:
            SocketHandler.Close();
        }

        public void StartListening()
        {
            _listening = true;

            ListeningThread = new Thread(DoListening);
            ListeningThread.Start();
        }

        private void DoListening()
        {
            while (_listening)
            {
                // Set state to unsignaled:
                ReceiveWaitHandle.Reset();

                // Create state object to save data read:
                StateObject state = new StateObject(Connection, SocketHandler);

                // Begin to asynchronously receive data:
                SocketHandler.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0, ReceiveCallback, state);

                // Wait for signal from ReceiveCallback to continue:
                ReceiveWaitHandle.WaitOne();
            }
        }

        private void ReceiveCallback(IAsyncResult result)
        {
            StateObject state = (StateObject)result.AsyncState;
            Socket socket = state.Socket;

            if (!IsConnected)
            {
                if (Disconnected != null)
                    Disconnected(this, EventArgs.Empty);

                return;
            }

            // End asynchronous receive:
            Int32 bytesRead;

            // Read from sockets:
            bytesRead = socket.EndReceive(result);

            // Signal thread in StartListening to continue:
            ReceiveWaitHandle.Set();

            if (bytesRead > 0)
            {
                // Decode received bytes into a string:
                state.Data = Encoding.UTF8.GetString(state.Buffer, 0, bytesRead);

                // Notify that data was received:
                if (DataReceived != null)
                    DataReceived(this, new StateChangedEventArgs(state));
            }
        }

        public void StopListening()
        {
            // Stop loop in StartListening: 
            _listening = false;

            // Signal thread blocked in StartListening to continue:
            ReceiveWaitHandle.Set();

            ListeningThread.Join();
        }

        public Boolean SendData(String data)
        {
            // Encode string into a sequence of bytes:
            Byte[] byteData = Encoding.UTF8.GetBytes(data);

            SendWaitHandle.Reset();

            // Begin asynchronous send:
            try
            {
                SocketHandler.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, SocketHandler);
                SendWaitHandle.WaitOne();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void SendCallback(IAsyncResult result)
        {
            Socket socketHandler = (Socket)result.AsyncState;

            // End asynchronous send:
            socketHandler.EndSend(result);

            SendWaitHandle.Set();

            StateObject state = new StateObject(Connection, socketHandler);

            // Notify that data was sent:
            if (DataSent != null)
                DataSent(this, new StateChangedEventArgs(state));
        }
    }
}