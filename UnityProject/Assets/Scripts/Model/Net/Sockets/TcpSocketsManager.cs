﻿    using System;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Model.Net.Sockets
{
    public class TcpSocketsManager
    {
        public class DataReceivedEventArgs : EventArgs
        {
            public Connection Connection { get; private set; }
            public String Data { get; private set; }

            public DataReceivedEventArgs(Connection connection, String data)
            {
                Connection = connection;
                Data = data;
            }
        }
        public class SocketDisconnectedEventArgs : EventArgs
        {
            public Connection Connection { get; private set; }

            public SocketDisconnectedEventArgs(Connection connection)
            {
                Connection = connection;
            }
        }

        public event EventHandler<DataReceivedEventArgs> DataReceived;
        public event EventHandler<SocketDisconnectedEventArgs> SocketDisconnected;

        public Dictionary<Connection, TcpSenderSocket> ConnectedSockets { get; private set; }
        private TcpListenerSocket ConnectionsListener { get; set; }
        public Boolean IsListening
        {
            get
            {
                return ConnectionsListener.IsListening;
            }
        }

        public TcpSocketsManager()
        {
            ConnectionsListener = new TcpListenerSocket();
            ConnectionsListener.SocketConnected += ConnectionsListener_SocketConnected;

            ConnectedSockets = new Dictionary<Connection, TcpSenderSocket>();
        }

        public void Initialize(String address, Int32 port)
        {
            lock (ConnectionsListener)
            {
                // Initialize listener socket on the specified port:
                ConnectionsListener.Initialize(address, port);

                // Start listening for incoming connections:
                ConnectionsListener.StartListening();
            }
        }

        public void Shutdown()
        {
            lock (ConnectionsListener)
            {
                // Stop listening for incoming connections:
                ConnectionsListener.StopListening();

                // Shutdown listener socket:
                ConnectionsListener.Shutdown();
            }

            lock (ConnectedSockets)
            {
                // For each connected socket in the set:
                foreach (var connectedSocket in ConnectedSockets.Values)
                {
                    // Stop listening for incoming data:
                    connectedSocket.StopListening();

                    // Shutdown socket:
                    connectedSocket.Shutdown();
                }
            }
        }

        public Boolean CreateSocket(Connection connection)
        {
            // Initialize a new socket:
            TcpSenderSocket socket = new TcpSenderSocket();
            if (!socket.Initialize(connection))
                return false;

            // Start listening for incoming data:
            socket.Disconnected += ConnectedSocket_Disconnected;
            socket.DataReceived += ConnectedSocket_DataReceived;
            socket.StartListening();

            // Add connected socket to dictionary:
            lock (ConnectedSockets)
            {
                ConnectedSockets.Add(connection, socket);
            }

            return true;
        }

        public Boolean DestroySocket(Connection connection)
        {
            TcpSenderSocket connectedSocket;

            lock (ConnectedSockets)
            {
                if (!ConnectedSockets.ContainsKey(connection))
                    return false;

                connectedSocket = ConnectedSockets[connection];
                ConnectedSockets.Remove(connection);
            }

            // Stop listening for incoming data:
            connectedSocket.StopListening();

            // Shutdown socket:
            connectedSocket.Shutdown();

            return true;
        }

        public void Send(Connection connection, String data)
        {
            TcpSenderSocket socket;

            lock (ConnectedSockets)
            {
                if (!ConnectedSockets.ContainsKey(connection))
                    return;

                socket = ConnectedSockets[connection];
            }

            // Send data through the connected socket:
            socket.SendData(data);
        }

        public void SendAll(String data)
        {
            lock (ConnectedSockets)
            {
                // For each connected socket in the set:
                foreach (var pair in ConnectedSockets)
                {
                    TcpSenderSocket connectedSocket = pair.Value;

                    // Send data through socket:
                    connectedSocket.SendData(data);
                }
            }
        }

        private void ConnectionsListener_SocketConnected(TcpListenerSocket sender, TcpListenerSocket.SocketConnectedEventArgs e)
        {
            // Initialize a new sender socket, using the socket that had just connected:
            TcpSenderSocket connectedSocket = new TcpSenderSocket();
            if (!connectedSocket.Initialize(e.SocketHandler))
                return;

            // Start listening for incoming data:
            connectedSocket.Disconnected += ConnectedSocket_Disconnected;
            connectedSocket.DataReceived += ConnectedSocket_DataReceived;
            connectedSocket.StartListening();

            // Add connected socket to dictionary:s
            Connection connection = new Connection(e.Address, e.Port);

            lock (ConnectedSockets)
            {
                ConnectedSockets.Add(connection, connectedSocket);
            }
        }

        private void ConnectedSocket_Disconnected(TcpSenderSocket sender, EventArgs e)
        {
            // Remove socket from dictionary:
            Connection connection = sender.Connection;

            lock (ConnectedSockets)
            {
                ConnectedSockets.Remove(connection);
            }

            // Shutdown socket:
            sender.StopListening();
            sender.Shutdown();

            // Notify that a socket was disconnected:
            if (SocketDisconnected != null)
                SocketDisconnected(this, new SocketDisconnectedEventArgs(connection));
        }

        private void ConnectedSocket_DataReceived(TcpSenderSocket sender, TcpSenderSocket.StateChangedEventArgs e)
        {
            if (DataReceived != null)
            {
                TcpSenderSocket.StateObject state = e.State;
                DataReceived(this, new DataReceivedEventArgs(state.Connection, state.Data));
            }
        }
    }
}