﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Raft
{
    public class LogEntry
    {
        /// <summary>
        /// Term when when entry was received by leader.
        /// </summary>
        public Int32 Term { get; private set; }

        /// <summary>
        /// Position of entry in the log.
        /// </summary>
        public Int32 Index { get; private set; }

        /// <summary>
        /// Command for state machine.
        /// </summary>
        public String Command { get; private set; }

        private Int32 HashCode { get; set; }

        public LogEntry(Int32 term, Int32 index, String command)
        {
            Term = term;
            Index = index;
            Command = command;

            HashCode = GenerateHashCode(term, index, command);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is LogEntry))
                return false;

            LogEntry entry = (LogEntry)obj;
            if (this != entry)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return HashCode;
        }

        public static bool operator ==(LogEntry a, LogEntry b)
        {
            if(a.Term != b.Term) 
                return false;
            if(a.Index != b.Index) 
                return false;
            if(a.Command != b.Command) 
                return false;

            return true;
        }

        public static bool operator !=(LogEntry a, LogEntry b)
        {
            return !(a == b);
        }

        private static Int32 GenerateHashCode(Int32 term, Int32 index, String command)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(term);
            builder.Append(";");
            builder.Append(index);
            builder.Append(";");
            builder.Append(command);

            return builder.ToString().GetHashCode();
        }
    }
}
