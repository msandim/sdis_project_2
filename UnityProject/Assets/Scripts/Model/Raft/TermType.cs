﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Raft
{
    public enum TermType
    {
        Old = -1, Same = 0, New = 1
    }
}
