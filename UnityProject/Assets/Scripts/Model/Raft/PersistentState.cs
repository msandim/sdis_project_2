﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Model.Raft
{
    using RPC;

    public class PersistentState
    {
        public class LeaderChangedEventArgs : EventArgs
        {
            public String LeaderID { get; private set; }

            public LeaderChangedEventArgs(String leaderID)
            {
                LeaderID = leaderID;
            }
        }
        public class MachineStateChangedEventArgs : EventArgs
        {
            public String Command { get; private set; }

            public MachineStateChangedEventArgs(String command)
            {
                Command = command;
            }

        }

        public static readonly PersistentState Emtpy = new PersistentState(String.Empty);

        public event EventHandler<LeaderChangedEventArgs> LeaderChanged;
        public event EventHandler<MachineStateChangedEventArgs> StateChanged;

        private String _leaderID;
        public String LeaderID
        {
            get
            {
                lock (_leaderIDLock)
                {
                    return _leaderID;
                }
            }
            set
            {
                String leaderID = value;

                lock (_leaderIDLock)
                {
                    _leaderID = leaderID;
                }

                if (LeaderChanged != null)
                    LeaderChanged(this, new LeaderChangedEventArgs(leaderID));
            }
        }

        /// <summary>
        /// Log's list initial capacity.
        /// </summary>
        private const Int32 _initialCapacity = 64;

        public String ServerID { get; private set; }

        /// <summary>
        /// Latest term server has seen (initialized to 0 on first boot). 
        /// </summary>
        private Int32 _currentTerm;
        public Int32 CurrentTerm
        {
            get
            {
                return _currentTerm;
            }
            set
            {
                _currentTerm = value;
                Console.WriteLine("Current Term: {0}", _currentTerm);
            }
        }

        /// <summary>
        /// Candidate ID that received vote in current term (null if none).
        /// </summary>
        public String VotedFor { get; set; }

        /// <summary>
        /// Log entries.
        /// </summary>
        public List<LogEntry> Log { get; private set; }

        public Int32 LogCount
        {
            get
            {
                lock(Log)
                {
                    return Log.Count;
                }
            }
        }

        public Int32 LastLogIndex
        {
            get
            {
                lock (Log)
                {
                    return Log.Count - 1;
                }
            }
        }

        public Int32 LastLogTerm
        {
            get
            {
                lock (Log)
                {
                    Int32 lastLogIndex = Log.Count - 1;

                    if (lastLogIndex < 0)
                        return -1;

                    return Log[lastLogIndex].Term;
                }
            }
        }

        private Int32 _commitIndex;
        public Int32 CommitIndex
        {
            get
            {
                lock(_commitIndexLock)
                {
                    return _commitIndex;
                }
            }
            set
            {
                lock(_commitIndexLock)
                {
                    _commitIndex = value;
                    Console.WriteLine("CommitIndex: {0}", _commitIndex);
                }
            }
        }

        private object _leaderIDLock = new object();
        protected object _currentTermLock = new object();
        protected object _commitIndexLock = new object();

        public PersistentState(String serverID)
        {
            ServerID = serverID;
            CurrentTerm = 0;
            VotedFor = null;
            LeaderID = null;
            CommitIndex = -1;

            // Create a list of log entries with an initial capacity:
            Log = new List<LogEntry>(_initialCapacity);
        }

        public void AddLogEntry(LogEntry entry)
        {
            lock(Log)
            {
                Log.Add(entry);
            }
        }

        public Int32 AddLogEntry(String command)
        {
            lock(this)
            {
                Int32 logIndex = Log.Count;

                Log.Add(new LogEntry(CurrentTerm, logIndex, command));

                return logIndex;
            }
        }

        public Boolean LogTermsMatch(Int32 index, Int32 term)
        {
            lock (Log)
            {
                // This is an exception to the rule, when log entry has index 0:
                if (index == -1 && term == -1)
                    return true;

                // Check if index is out of range:
                if (index >= Log.Count || index < 0)
                    return false;

                // Check if log entry's term matches the specified term:
                if (Log[index].Term != term)
                    return false;
            }

            return true;
        }

        public RequestVoteRPC CreateRequestVoteMessage()
        {
            lock (this)
            {
                return new RequestVoteRPC(
                ServerID,
                CurrentTerm,
                LastLogIndex,
                LastLogTerm
                );
            }
        }

        public RequestVoteReplyRPC CreateRequestVoteReplyMessage(Boolean voteGranted)
        {
            lock (_currentTermLock)
            {
                return new RequestVoteReplyRPC(
                CurrentTerm,
                voteGranted
                );
            }
        }

        public AppendEntriesRPC CreateAppendEntriesMessage(Int32 nextIndex)
        {
            lock (this)
            {
                // Create message:
                Int32 previousLogIndex = nextIndex - 1;
                Int32 previousLogTerm = previousLogIndex >= 0 ? Log[previousLogIndex].Term : -1;
                AppendEntriesRPC message = new AppendEntriesRPC(
                    CurrentTerm,
                    ServerID,
                    previousLogIndex,
                    previousLogTerm,
                    CommitIndex
                    );

                // Add entries:
                Int32 lastLogIndex = LastLogIndex;
                if (lastLogIndex >= nextIndex)
                    for (int i = nextIndex; i <= lastLogIndex; i++)
                        message.AddEntry(Log[i]);

                return message;
            }
        }

        public AppendEntriesReplyRPC CreateAppendEntriesReply(Boolean success)
        {
            lock (this)
            {
                return new AppendEntriesReplyRPC(
                    CurrentTerm,
                    success,
                    LastLogIndex
                    );
            }
        }

        public void UpdateStateMachine(Int32 newCommitIndex)
        {
            Int32 oldCommitIndex;

            lock (_commitIndexLock)
            {
                oldCommitIndex = CommitIndex;
                if (oldCommitIndex == newCommitIndex)
                    return;

                CommitIndex = newCommitIndex;
            }

            if (StateChanged == null)
                return;

            lock (Log)
            {
                for (Int32 i = oldCommitIndex + 1; i <= newCommitIndex; i++)
                    StateChanged(this, new MachineStateChangedEventArgs(Log[i].Command));
            }
        }

        public void CheckLogInconsistencies(AppendEntriesRPC message)
        {
            lock (this)
            {
                foreach(var messageEntry in message.Entries)
                {
                    // Append entry not already in the log:
                    if (messageEntry.Index > LastLogIndex)
                    {
                        Log.Add(messageEntry);
                        continue;
                    }

                    LogEntry localEntry = Log[messageEntry.Index];

                    // If local entry is different from message's entry:
                    if (localEntry != messageEntry)
                    {
                        // Replace local entry:
                        Log[messageEntry.Index] = messageEntry;
                    }
                }

                // If local log has more entries than message:
                if (Log.Count > message.Entries.Count)
                {
                    Int32 begin = message.PreviousLogIndex + 1;
                    Int32 end = begin + message.Entries.Count;
                    Int32 removeBegin = end;
                    Int32 removeCount = Log.Count - removeBegin;

                    // Remove extra entries:
                    Log.RemoveRange(removeBegin, removeCount);
                }
            }
        }

        public void VoteForSelf()
        {
            lock (this)
            {
                CurrentTerm += 1;
                VotedFor = ServerID;
            }
        }

        public Boolean CandidateLogIsComplete(RequestVoteRPC message)
        {
            Int32 lastLogTerm;
            Int32 lastLogIndex;

            lock (Log)
            {
                lastLogTerm = LastLogTerm;
                lastLogIndex = LastLogIndex;
            }

            Int32 candidateLastLogIndex = message.LastLogIndex;
            Int32 candidateLastLogTerm = message.LastLogTerm;

            // If follower's log is "more complete", then candidate's log is less complete:
            if (lastLogTerm > candidateLastLogTerm ||
                (lastLogTerm == candidateLastLogTerm && lastLogIndex > candidateLastLogIndex))
                return false;

            return true;
        }

        public TermType UpdateTerm(Int32 term)
        {
            lock (_currentTermLock)
            {
                if (term < CurrentTerm)
                    return TermType.Old;

                if (term > CurrentTerm)
                {
                    CurrentTerm = term;
                    return TermType.New;
                }

                return TermType.Same;
            }
        }

        public TermType UpdateLeaderAndTerm(String serverID, Int32 term)
        {
            TermType result = UpdateTerm(term);

            // Update leader ID if new term or same term, but different leader:
            if (result == TermType.New
                || (result == TermType.Same && serverID != LeaderID))
                LeaderID = serverID;

            return result;
        }
    }
}
