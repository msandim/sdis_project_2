﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Model.Raft
{
    using State;
    using Net;
    using Assets.Scripts.Model.Raft.RPC;

    public class RaftServer
    {
        public class StateChangedEventArgs : EventArgs
        {
            public AbstractState.Type State { get; private set; }
            public Int32 CurrentTerm { get; private set; }

            public StateChangedEventArgs(AbstractState.Type state, Int32 currentTerm)
            {
                State = state;
                CurrentTerm = currentTerm;
            }
        }

        public event EventHandler<StateChangedEventArgs> StateChanged;
        public event EventHandler<PersistentState.LeaderChangedEventArgs> LeaderChanged;
        public event EventHandler<PersistentState.MachineStateChangedEventArgs> MachineStateChanged;

        public AbstractState.Type Status { get; private set; }

        private INetwork Network { get; set; }
        private AbstractState CurrentState { get; set; }
        private FollowerState FollowerState { get; set; }
        private CandidateState CandidateState { get; set; }
        private LeaderState LeaderState { get; set; }

        public RaftServer()
        {
            CurrentState = null;
            FollowerState = new FollowerState();
            CandidateState = new CandidateState();
            LeaderState = new LeaderState();
        }

        public void Initialize(INetwork network)
        {
            Network = network;

            // Create a new persistent state:
            PersistentState persistentState = new PersistentState(Network.ID);
            persistentState.LeaderChanged += PersistentState_LeaderChanged;
            persistentState.StateChanged += PersistentState_StateChanged;

            ChangeState(AbstractState.Type.Follower, persistentState);
        }

        public void Shutdown()
        {
            lock(this)
            {
                if(CurrentState != null)
                    CurrentState.Shutdown();

                CurrentState.StateChanged -= PersistentState_StateChanged;
                CurrentState.LeaderChanged -= PersistentState_LeaderChanged;
            }
        }

        public void SendClientRequest(String command)
        {
            ClientRequest request = new ClientRequest(command);

            lock(CurrentState)
            {
                if (CurrentState == LeaderState)
                {
                    LeaderState.HandleClientRequest(command);
                    return;
                }

                if (CurrentState.PersistentState.LeaderID == null)
                    return;
            }

            lock(Network)
            {
                // Send request to leader:
                Network.Send(CurrentState.PersistentState.LeaderID, request.Serialize());
            }
        }

        public void ClearState()
        {
            if(CurrentState != null)
            {
                lock(CurrentState)
                {
                    CurrentState.ClearState();
                }
            }
        }

        protected void ChangeState(AbstractState.Type newType, PersistentState persistentState)
        {
            lock(this)
            {
                if (CurrentState != null)
                {
                    // Shutdown previous state:
                    CurrentState.Shutdown();
                }

                // Find state which corresponds to enum AbstractState.Type:
                AbstractState newState;
                if (!FindState(newType, out newState))
                    return;

                // Initialize new state:
                newState.Initialize(persistentState, Network, ChangeState);

                // Set current state:
                CurrentState = newState;
                Status = newType;

                // Notify that raft state changed:
                if (StateChanged != null)
                    StateChanged(this, new StateChangedEventArgs(newType, persistentState.CurrentTerm));
            }
        }

        /// <summary>
        /// For test porposes.
        /// </summary>
        /// <param name="newType"></param>
        protected void DoNothing(AbstractState.Type newType, PersistentState persistentState)
        {
        }

        private Boolean FindState(AbstractState.Type type, out AbstractState state)
        {
            state = null;

            switch (type)
            {
                case AbstractState.Type.Follower:
                    state = FollowerState;
                    break;
                case AbstractState.Type.Candidate:
                    state = CandidateState;
                    break;
                case AbstractState.Type.Leader:
                    state = LeaderState;
                    break;
                default:
                    return false;
            }

            return true;
        }

        private void PersistentState_LeaderChanged(object sender, PersistentState.LeaderChangedEventArgs e)
        {
            if (LeaderChanged != null)
                LeaderChanged(this, e);
        }

        private void PersistentState_StateChanged(object sender, PersistentState.MachineStateChangedEventArgs e)
        {
            if (MachineStateChanged != null)
                MachineStateChanged(this, e);
        }
    }
}
