﻿using System;
using System.Timers;

namespace Assets.Scripts.Model.Raft.State
{
    using Net;
    using Net.Messages;
    using RPC;

    public class CandidateState : AbstractState
    {
        private const Int32 _electionTimeout = 2000;

        private Int32 MyVotes { get; set; }
        private Timer ElectionTimeoutTimer { get; set; }
        private Random RandomGenerator { get; set; }
        private Int32 NumberConnectedServers
        {
            get
            {
                lock (Network)
                {
                    return Network.NumberServers;
                }
            }
        }
        private object _myVotesLock = new object();

        public CandidateState()
        {
            ElectionTimeoutTimer = new Timer();
            RandomGenerator = new Random();

            ElectionTimeoutTimer.Elapsed += ElectionTimeoutTimer_Elapsed;
        }

        public override void Initialize(PersistentState persistentState, INetwork network, ChangeStateHandler changeState)
        {
            base.Initialize(persistentState, network, changeState);

            // Subscribe to new data received and handle:
            Network.DataReceived += Network_DataReceived;

            // Start new election on next update:
            StartNewElection();

            // Start election timeout timer:
            ElectionTimeoutTimer.Start();
        }

        public override void Shutdown()
        {
            // Stop timer:
            ElectionTimeoutTimer.Stop();

            // Unsubscribe to net manager events:
            Network.DataReceived -= Network_DataReceived;

            base.Shutdown();
        }

        private void ElectionTimeoutTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // If there are no connected servers:
            if (NumberConnectedServers == 0)
            {
                // Become leader:
                ChangeState(Type.Leader, PersistentState);
            }
            else
            {
                // Start new election:
                StartNewElection();
            }
        }

        private void StartNewElection()
        {
            lock (ElectionTimeoutTimer)
            {
                // Reset timeout timer:
                ElectionTimeoutTimer.Interval = GenerateNewTimeout();
            }

            // Increment current term and set VotedFor:
            PersistentState.VoteForSelf();

            lock(_myVotesLock)
            {
                // Reset my votes to 0:
                MyVotes = 0;
            }

            // Send request votes to all servers:
            SendRequestVotes();
        }

        private void SendRequestVotes()
        {
            RequestVoteRPC message = PersistentState.CreateRequestVoteMessage();

            lock (Network)
            {
                // Send message to all connected servers:
                Network.SendAll(message.Serialize());
            }
        }

        private void Network_DataReceived(object sender, DataReceivedEventArgs e)
        {
            String data = e.Data;

            // Ignore mal formed messages:
            String type;
            if (!Message.FindType(data, out type))
                return;

            // Handle request vote:
            if (type == RequestVoteRPC.StaticType)
                HandleRequestVote(RequestVoteRPC.Deserialize(data));

            // Handle request vote reply:
            else if (type == RequestVoteReplyRPC.StaticType)
                HandleRequestVoteReply(e.ServerID, RequestVoteReplyRPC.Deserialize(data));

            // Handle append entries:
            else if (type == AppendEntriesRPC.StaticType)
                HandleAppendEntries(AppendEntriesRPC.Deserialize(data));
        }

        protected override void HandleAppendEntries(AppendEntriesRPC message)
        {
            Console.WriteLine(message.Serialize());

            // Update term and leader ID if needed:
            TermType result = PersistentState.UpdateLeaderAndTerm(message.LeaderID, message.Term);

            // Ignore messages of old terms:
            if (result == TermType.Old)
                return;

            lock (ElectionTimeoutTimer)
            {
                // Immediately stop election:
                ElectionTimeoutTimer.Stop();
            }

            // Check conflicts in log, update state machine, send reply:
            base.HandleAppendEntries(message);

            // Step down as follower:
            ChangeState(Type.Follower, PersistentState);
        }

        protected void HandleRequestVote(RequestVoteRPC message)
        {
            // Update term if needed:
            TermType result = PersistentState.UpdateTerm(message.Term);

            // Return if message of old term:
            if (result == TermType.Old)
                return;

            // If message comes from a new term:
            if (result == TermType.New)
            {
                lock (ElectionTimeoutTimer)
                {
                    // Immediately stop election:
                    ElectionTimeoutTimer.Stop();
                }
            }

            // Decide vote for candidate:
            base.HandleRequestVote(message, termType: result);

            // If message comes from a new term:
            if (result == TermType.New)
            {
                // Step down as follower:
                ChangeState(Type.Follower, PersistentState);
            }
        }

        private void HandleRequestVoteReply(String serverID, RequestVoteReplyRPC message)
        {
            if (message.VoteGranted)
            {
                Boolean electionsWon;

                lock (_myVotesLock)
                {
                    // one more vote!!
                    MyVotes++;

                    // are we elected leader yet?
                    electionsWon = CheckElectedLeader();
                }

                if (electionsWon)
                    ChangeState(Type.Leader, PersistentState);
            }
        }

        private bool CheckElectedLeader()
        {
            return MyVotes >= (NumberConnectedServers + 1) / 2;
        }

        private Int32 GenerateNewTimeout()
        {
            lock (RandomGenerator)
            {
                // Generate a timeout between T and 2T:
                return RandomGenerator.Next(_electionTimeout, _electionTimeout * 2);
            }
        }
    }
}
