﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace Assets.Scripts.Model.Raft.State
{
    using Net;
    using Net.Messages;
    using RPC;

    public class FollowerState : AbstractState
    {
        private const Int32 _electionTimeout = 2000;

        private Timer ElectionTimeoutTimer { get; set; }

        public FollowerState()
        {
            ElectionTimeoutTimer = new Timer();

            ElectionTimeoutTimer.Elapsed += ElectionTimeoutTimer_Elapsed;
        }

        /// <summary>
        /// Initialize state, subscribe to NetManager events and start election timeout.
        /// </summary>
        /// <param name="id"
        /// <param name="persistentState"></param>
        /// <param name="netManager"></param>
        /// <param name="changeState"></param>
        public override void Initialize(PersistentState persistentState, INetwork network, ChangeStateHandler changeState)
        {
            base.Initialize(persistentState, network, changeState);

            // Subscribe to net manager events:
            Network.DataReceived += Network_DataReceived;

            // Reset timer timeout interval:
            ElectionTimeoutTimer.Interval = _electionTimeout;

            // Start timer:
            ElectionTimeoutTimer.Start();
        }

        public override void Shutdown()
        {
            // Stop timer:
            ElectionTimeoutTimer.Stop();

            // Unsubscribe to net manager events:
            Network.DataReceived -= Network_DataReceived;

            base.Shutdown();
        }

        /// <summary>
        /// Called when an election timeout occurs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ElectionTimeoutTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Step up as candidate:
            base.ChangeState(Type.Candidate, PersistentState);
        }

        private void Network_DataReceived(object sender, DataReceivedEventArgs e)
        {
            lock (ElectionTimeoutTimer)
            {
                // Reset election timeout:
                if (ElectionTimeoutTimer.Enabled)
                    ElectionTimeoutTimer.Interval = _electionTimeout;
            }

            String data = e.Data;

            // Ignore mal formed messages:
            String type;
            if (!Message.FindType(data, out type))
                return;

            // Handle Append Entries RPC:
            if (type == AppendEntriesRPC.StaticType)
                HandleAppendEntries(AppendEntriesRPC.Deserialize(data));

            // Handle Request Vote RPC:
            else if (type == RequestVoteRPC.StaticType)
                HandleRequestVote(RequestVoteRPC.Deserialize(data));
        }

        protected override void HandleAppendEntries(AppendEntriesRPC message)
        {
            Console.WriteLine(message.Serialize());

            // Update term and leader ID if needed:
            TermType result = PersistentState.UpdateLeaderAndTerm(message.LeaderID, message.Term);

            // Ignore messages of old terms:
            if (result == TermType.Old)
                return;

            lock (ElectionTimeoutTimer)
            {
                // Reset election timeout:
                ElectionTimeoutTimer.Interval = _electionTimeout;
            }

            // Check conflicts in log, update state machine, send reply:
            base.HandleAppendEntries(message);
        }

        protected void HandleRequestVote(RequestVoteRPC message)
        {
            // Update term if needed:
            TermType result = PersistentState.UpdateTerm(message.Term);

            // Return if message of old term:
            if (result == TermType.Old)
                return;

            // Decide vote for candidate:
            Boolean voted = base.HandleRequestVote(message, termType: result);

            if(voted)
            {
                lock (ElectionTimeoutTimer)
                {
                    // Reset election timeout:
                    ElectionTimeoutTimer.Interval = _electionTimeout;
                }
            }
        }
    }
}