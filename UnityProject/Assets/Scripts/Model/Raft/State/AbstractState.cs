﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Model.Raft.State
{
    using Net;
    using RPC;

    public abstract class AbstractState
    {
        public enum Type
        {
            Follower = 0,
            Candidate = 1,
            Leader = 2
        }

        public delegate void ChangeStateHandler(Type newState, PersistentState persistentState);

        public event EventHandler<PersistentState.LeaderChangedEventArgs> LeaderChanged;
        public event EventHandler<PersistentState.MachineStateChangedEventArgs> StateChanged;

        protected ChangeStateHandler ChangeState { get; set; }
        protected INetwork Network { get; set; }

        public PersistentState PersistentState { get; set; }
        protected String VotedFor
        {
            get
            {
                return PersistentState.VotedFor;
            }
            set
            {
                PersistentState.VotedFor = value;
            }
        }
        protected Int32 LastLogIndex
        {
            get
            {
                return PersistentState.LastLogIndex;
            }
        }
        protected Int32 CommitIndex
        {
            get
            {
                return PersistentState.CommitIndex;
            }
            set
            {
                PersistentState.CommitIndex = value;
            }
        }

        /// <summary>
        /// Initialize state.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="persistentState"></param>
        /// <param name="network"></param>
        /// <param name="changeState"></param>
        public virtual void Initialize(PersistentState persistentState, INetwork network, ChangeStateHandler changeState)
        {
            PersistentState = persistentState;
            Network = network;
            ChangeState = changeState;

            PersistentState.StateChanged += PersistentState_StateChanged;
        }

        /// <summary>
        /// </summary>
        public virtual void Shutdown()
        {
        }

        public virtual void ClearState()
        {
            lock(PersistentState)
            {
                PersistentState.Log.Clear();
                PersistentState.CommitIndex = -1;
            }
        }

        protected virtual void HandleAppendEntries(AppendEntriesRPC message)
        {
            // Return failure if log doesn't contain an entry at previousLogIndex whose term matches previousLogTerm:
            if (!PersistentState.LogTermsMatch(message.PreviousLogIndex, message.PreviousLogTerm))
            {
                SendAppendEntriesReply(message.LeaderID, success: false);
                return;
            }

            // Check conflicts in log:
            PersistentState.CheckLogInconsistencies(message);

            // Advance state machine with commited entries:
            PersistentState.UpdateStateMachine(message.CommitIndex);

            // Send success reply:
            SendAppendEntriesReply(message.LeaderID, success: true);
        }

        protected Boolean HandleRequestVote(RequestVoteRPC message, TermType termType)
        {
            lock (PersistentState)
            {
                // If new election:
                if (termType == TermType.New)
                    VotedFor = null;

                // If candidate's log is "less complete", then deny vote:
                if (!PersistentState.CandidateLogIsComplete(message))
                {
                    SendRequestVoteReply(message.CandidateID, voteGranted: false);
                    return false;
                }

                // If first candidate with a "complete" log:
                else if (VotedFor == null)
                {
                    // Vote for this candidate:
                    VotedFor = message.CandidateID;
                    SendRequestVoteReply(message.CandidateID, voteGranted: true);
                    return true;
                }
                else
                {
                    // Deny vote:
                    SendRequestVoteReply(message.CandidateID, voteGranted: false);
                    return false;
                }
            }
        }

        private void SendAppendEntriesReply(String leaderID, Boolean success)
        {
            AppendEntriesReplyRPC reply = PersistentState.CreateAppendEntriesReply(success);

            lock (Network)
            {
                Network.Send(leaderID, reply.Serialize());
            }
        }

        private void SendRequestVoteReply(String serverID, Boolean voteGranted)
        {
            RequestVoteReplyRPC reply = PersistentState.CreateRequestVoteReplyMessage(voteGranted);

            Network.Send(serverID, reply.Serialize());
        }

        private void PersistentState_StateChanged(object sender, PersistentState.MachineStateChangedEventArgs e)
        {
            if (StateChanged != null)
                StateChanged(this, e);
        }
    }
}
