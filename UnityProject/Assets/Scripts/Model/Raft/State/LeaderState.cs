﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace Assets.Scripts.Model.Raft.State
{
    using Net;
    using Net.Messages;
    using RPC;

    public class LeaderState : AbstractState
    {
        private class SuccessCounter
        {
            public Int32 Index { get; private set; }
            public Int32 Count { get; set; }

            public SuccessCounter(Int32 index)
            {
                Index = index;
                Count = 0;
            }
        }

        private const Int32 _heartBeatInterval = 200;

        private Timer HeartBeatTimer { get; set; }
        private Dictionary<String, Int32> NextIndices { get; set; }
        private Int32 NumberFollowers
        {
            get
            {
                lock (NextIndices)
                {
                    return NextIndices.Count;
                }
            }
        }
        private Queue<SuccessCounter> SuccessCounters { get; set; }

        public LeaderState()
        {
            HeartBeatTimer = new Timer();
            NextIndices = new Dictionary<String, int>();
            SuccessCounters = new Queue<SuccessCounter>();

            // Subscibe to elapsed events:
            HeartBeatTimer.Elapsed += HeartBeatTimer_Elapsed;
        }

        public override void Initialize(PersistentState persistentState, INetwork network, ChangeStateHandler changeState)
        {
            base.Initialize(persistentState, network, changeState);

            // Subscribe to network events:
            Network.ServerConnected += Network_ServerConnected;
            Network.ServerDisconnected += Network_ServerDisconnected;
            Network.DataReceived += Network_DataReceived;

            // Set leader ID:
            PersistentState.LeaderID = Network.ID;

            // Initialize next indices with an initial value equals to lastLogIndex + 1:
            Int32 initialNextIndex = LastLogIndex + 1;

            NextIndices.Clear();
            foreach (var serverID in Network.GetServers())
                NextIndices[serverID] = initialNextIndex;

            // Initialize queue of success counters:
            Int32 logCount = PersistentState.LogCount;
            SuccessCounters.Clear();
            for (Int32 index = CommitIndex + 1; index < logCount; index++)
                SuccessCounters.Enqueue(new SuccessCounter(index));

            // Send initial heart beat:
            SendAppendEntries();

            // Set heart beat interval:
            HeartBeatTimer.Interval = _heartBeatInterval;

            // Start timer:
            HeartBeatTimer.Start();
        }

        public override void Shutdown()
        {
            // Stop timer:
            HeartBeatTimer.Stop();

            // Unsubscribe to net manager events:
            Network.DataReceived -= Network_DataReceived;
            Network.ServerDisconnected -= Network_ServerDisconnected;
            Network.ServerConnected -= Network_ServerConnected;

            base.Shutdown();
        }

        public void HandleClientRequest(String command)
        {
            // Add log entry:
            Int32 logIndex = PersistentState.AddLogEntry(command);

            lock (SuccessCounters)
            {
                // Add success counter:
                SuccessCounters.Enqueue(new SuccessCounter(logIndex));
            }
        }

        public override void ClearState()
        {
            lock(this)
            {
                base.ClearState();

                SuccessCounters.Clear();
                NextIndices.Clear();
            }
        }

        private void HeartBeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            UpdateCommitIndex();

            SendAppendEntries();
        }

        private void UpdateCommitIndex()
        {
            Int32 newCommitIndex;

            lock (PersistentState)
            {
                newCommitIndex = CommitIndex;
            }

            Int32 successLimit = NumberFollowers / 2;

            lock (SuccessCounters)
            {
                // While it is possible to remove elements from queue:
                while (SuccessCounters.Count != 0)
                {
                    SuccessCounter success = SuccessCounters.Peek();

                    // If entry can be committed:
                    if (success.Count >= successLimit)
                    {
                        // Remove element from queue:
                        SuccessCounters.Dequeue();

                        // Commit entry:
                        newCommitIndex++;

                        // Update state machine:
                        PersistentState.UpdateStateMachine(newCommitIndex);
                    }
                    else
                    {
                        // Break cicle since no other elements can be removed:
                        break;
                    }
                }
            }
        }

        private void SendAppendEntries()
        {
            lock (NextIndices)
            {
                foreach (var pair in NextIndices)
                {
                    String serverID = pair.Key;
                    Int32 nextIndex = pair.Value;

                    // Create append entries message:
                    AppendEntriesRPC message = PersistentState.CreateAppendEntriesMessage(nextIndex);

                    lock (Network)
                    {
                        // Send append entries RPC:
                        Network.Send(serverID, message.Serialize());
                    }
                }
            }
        }

        private void Network_ServerConnected(object sender, ServerEventArgs e)
        {
            String serverID = e.ServerID;

            // Initialize next index to last log index + 1:
            Int32 nextIndex = PersistentState.LogCount;

            lock (NextIndices)
            {
                if (NextIndices.ContainsKey(serverID))
                {
                    NextIndices[serverID] = nextIndex;
                    return;
                }

                NextIndices.Add(serverID, nextIndex);
            }
        }

        private void Network_ServerDisconnected(object sender, ServerEventArgs e)
        {
            lock (NextIndices)
            {
                NextIndices.Remove(e.ServerID);
            }
        }

        private void Network_DataReceived(object sender, DataReceivedEventArgs e)
        {
            String data = e.Data;

            // Ignore mal formed messages:
            String type;
            if (!Message.FindType(data, out type))
                return;

            // Handle append entries:
            if (type == AppendEntriesRPC.StaticType)
                HandleAppendEntries(AppendEntriesRPC.Deserialize(data));

            // Handle append entries reply:
            else if (type == AppendEntriesReplyRPC.StaticType)
                HandleAppendEntriesReply(e.ServerID, AppendEntriesReplyRPC.Deserialize(data));

            // Handle request vote:
            else if (type == RequestVoteRPC.StaticType)
                HandleRequestVote(RequestVoteRPC.Deserialize(data));

            // Handle commands from clients:
            else if (type == ClientRequest.StaticType)
                HandleClientRequest(ClientRequest.Deserialize(data));
        }

        protected override void HandleAppendEntries(AppendEntriesRPC message)
        {
            Console.WriteLine(message.Serialize());

            // Update term and leader ID if needed:
            TermType result = PersistentState.UpdateLeaderAndTerm(message.LeaderID, message.Term);

            // Ignore messages of old terms:
            if (result == TermType.Old)
                return;

            lock (HeartBeatTimer)
            {
                // Immediately stop sending heartbeats: 
                HeartBeatTimer.Stop();
            }

            // Check conflicts in log, update state machine, send reply:
            base.HandleAppendEntries(message);

            // Step down as follower:
            ChangeState(Type.Follower, PersistentState);
        }

        private void HandleAppendEntriesReply(String followerID, AppendEntriesReplyRPC message)
        {
            String serverID = followerID;
            Int32 nextIndex;

            // If not success:
            if (!message.Success)
            {
                lock (NextIndices)
                {
                    // Decrement next index:
                    nextIndex = NextIndices[serverID] - 1;

                    // Next index must be greater than 0:
                    if (nextIndex < 0)
                        nextIndex = 0;
                    else
                        NextIndices[serverID] = nextIndex;
                }

                // Create a new append entries message with new next index:
                AppendEntriesRPC reply = PersistentState.CreateAppendEntriesMessage(nextIndex);

                lock (Network)
                {
                    // Send reply:
                    Network.Send(serverID, reply.Serialize());
                }

                return;
            }

            lock (NextIndices)
            {
                // Update next index since append entries was successful:
                nextIndex = message.LastLogIndex + 1;
                NextIndices[serverID] = nextIndex;
            }

            lock (SuccessCounters)
            {
                // Update success count:
                foreach (var successCounter in SuccessCounters)
                {
                    if (successCounter.Index >= nextIndex)
                        break;

                    successCounter.Count += 1;
                }
            }
        }

        protected void HandleRequestVote(RequestVoteRPC message)
        {
            // Update term if needed:
            TermType result = PersistentState.UpdateTerm(message.Term);

            // Return if message of old term:
            if (result == TermType.Old)
                return;

            // If message comes from a new term:
            if (result == TermType.New)
            {
                lock (HeartBeatTimer)
                {
                    // Immediately stop sending heart beats:
                    HeartBeatTimer.Stop();
                }
            }

            // Decide vote for candidate:
            base.HandleRequestVote(message, termType: result);

            // If message comes from a new term:
            if (result == TermType.New)
            {
                // Step down as follower:
                ChangeState(Type.Follower, PersistentState);
            }
        }

        private void HandleClientRequest(ClientRequest message)
        {
            HandleClientRequest(message.Command);
        }
    }
}