﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Raft.RPC
{
    using Net.Messages;

    /// <summary>
    /// Invoked by candidates to gather votes.
    /// </summary>
    public class RequestVoteRPC : Message
    {
        public static readonly String StaticType = "RequestVoteRPC";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        /// <summary>
        /// Candidate requesting vote.
        /// </summary>
        public String CandidateID { get; private set; }

        /// <summary>
        /// Candidate's term.
        /// </summary>
        public Int32 Term { get; private set; }

        /// <summary>
        /// Index of candidate's last log entry.
        /// </summary>
        public Int32 LastLogIndex { get; private set; }

        /// <summary>
        /// Term of candidate's last log entry.
        /// </summary>
        public Int32 LastLogTerm { get; private set; }

        public RequestVoteRPC(String candidateID, Int32 term, Int32 lastLogIndex, Int32 lastLogTerm)
        {
            CandidateID = candidateID;
            Term = term;
            LastLogIndex = lastLogIndex;
            LastLogTerm = lastLogTerm;
        }

        public static RequestVoteRPC Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<RequestVoteRPC>(jsonValue);
        }
    }
}
