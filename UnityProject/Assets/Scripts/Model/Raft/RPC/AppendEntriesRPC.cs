﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Model.Raft.RPC
{
    using Net.Messages;

    /// <summary>
    /// Invoked by leader to replicate log entries and discover inconsistencies.
    /// It is also used as heartbeat.
    /// </summary>
    public class AppendEntriesRPC : Message
    {
        public static readonly String StaticType = "AppendEntriesRPC";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        /// <summary>
        /// Leader's term.
        /// </summary>
        public Int32 Term { get; private set; }

        /// <summary>
        /// Leader ID so followers can redirect clients.
        /// </summary>
        public String LeaderID { get; private set; }

        /// <summary>
        /// Index of log entry immediately preceding new ones.
        /// </summary>
        public Int32 PreviousLogIndex { get; private set; }

        /// <summary>
        /// Term of PreviousLogIndex entry.
        /// </summary>
        public Int32 PreviousLogTerm { get; private set; }

        /// <summary>
        /// Log entries to store (empty for heartbeat).
        /// </summary>
        public List<LogEntry> Entries { get; private set; }

        /// <summary>
        /// Last entry known to be committed.
        /// </summary>
        public Int32 CommitIndex { get; private set; }

        public AppendEntriesRPC(Int32 term, String leaderID, Int32 previousLogIndex, Int32 previousLogTerm, Int32 commitIndex)
        {
            Term = term;
            LeaderID = leaderID;
            PreviousLogIndex = previousLogIndex;
            PreviousLogTerm = previousLogTerm;
            Entries = new List<LogEntry>();
            CommitIndex = commitIndex;
        }

        public void AddEntry(LogEntry entry)
        {
            Entries.Add(entry);
        }

        public static AppendEntriesRPC Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<AppendEntriesRPC>(jsonValue);
        }
    }
}
