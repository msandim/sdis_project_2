﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Raft.RPC
{
    using Net.Messages;

    public class RequestVoteReplyRPC : Message
    {
        public static readonly String StaticType = "ReplyVoteRPC";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        /// <summary>
        /// Current term, for candidate to update itself.
        /// </summary>
        public Int32 Term { get; private set; }

        /// <summary>
        /// True means candidate received vote.
        /// </summary>
        public Boolean VoteGranted { get; private set; }

        public RequestVoteReplyRPC(Int32 term, Boolean voteGranted)
        {
            Term = term;
            VoteGranted = voteGranted;
        }

        public static RequestVoteReplyRPC Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<RequestVoteReplyRPC>(jsonValue);
        }
    }
}
