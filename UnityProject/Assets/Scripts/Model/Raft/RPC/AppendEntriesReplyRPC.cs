﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Raft.RPC
{
    using Net.Messages;

    public class AppendEntriesReplyRPC : Message
    {
        public static readonly String StaticType = "AppendEntriesReplyRPC";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        /// <summary>
        /// Current term, for leader to update itself.
        /// </summary>
        public Int32 Term { get; private set; }

        /// <summary>
        /// True if follower contained entry matching prevLogIndex and prevLogTerm.
        /// </summary>
        public Boolean Success { get; private set; }

        public Int32 LastLogIndex { get; private set; }

        public AppendEntriesReplyRPC(Int32 term, Boolean success, Int32 lastLogIndex)
        {
            Term = term;
            Success = success;
            LastLogIndex = lastLogIndex;
        }

        public static AppendEntriesReplyRPC Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<AppendEntriesReplyRPC>(jsonValue);
        }
    }
}
