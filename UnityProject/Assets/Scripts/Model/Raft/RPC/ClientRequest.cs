﻿using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Raft.RPC
{
    using Net.Messages;

    public class ClientRequest : Message
    {
        public static readonly String StaticType = "ClientRequest";
        public override string Type
        {
            get
            {
                return StaticType;
            }
        }

        public String Command { get; private set; }

        public ClientRequest(String command)
        {
            Command = command;
        }

        public static ClientRequest Deserialize(String jsonValue)
        {
            return JsonConvert.DeserializeObject<ClientRequest>(jsonValue);
        }
    }
}
