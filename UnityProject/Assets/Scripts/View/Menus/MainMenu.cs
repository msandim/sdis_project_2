﻿using System;
using UnityEngine;

namespace Assets.Scripts.View.Menus
{
    public class MainMenu : MonoBehaviour
    {
        public void LoadCreateRoomMenu()
        {
            Application.LoadLevel("CreateRoomMenu");
        }

        public void LoadJoinRoomMenu()
        {
            Application.LoadLevel("JoinRoomMenu");
        }
    }
}
