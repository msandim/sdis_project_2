﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View.Menus.CreateRoom
{
    using Model;
    using Model.Logic;
    using Model.Net;
    using Model.Net.Sockets;

    public class CreateRoomMenu : MonoBehaviour
    {
        public InputField p_localPortField;
        public Text p_statusText;
        public Button p_topButton;
        public Button p_bottomButton;
        public Text p_serverInformationText;
        public Text p_connectedPlayersText;

        private Int32 _localPort;
        private String LocalPort
        {
            get
            {
                return _localPort.ToString();
            }
            set
            {
                Int32.TryParse(value, out _localPort);
            }
        }

        private Text TopButtonText { get; set; }
        private Text BottomButtonText { get; set; }

        private GameModel GameModel { get; set; }
        private Boolean GameInitialized { get; set; }

        public void Start()
        {
            var gameController = GameController.Instance;
            GameModel = gameController.GameModel;

            TopButtonText = p_topButton.GetComponentInChildren<Text>();
            BottomButtonText = p_bottomButton.GetComponentInChildren<Text>();

            GameModel.GameInitialized += GameModel_GameInitialized;
        }

        private void GameModel_GameInitialized(object sender, GameLogic.GameInitializedEventArgs e)
        {
            lock(this)
            {
                // When the game is initialized, then we can start the game:
                GameModel.StartGame();

                GameInitialized = true;
            }
        }

        public void OnListenPortFieldEndEdit(String text)
        {
            LocalPort = p_localPortField.text;
            p_localPortField.text = LocalPort;
        }

        public void OnTopButtonClick()
        {
            if (GameModel.NetworkStatus == NetworkStatus.Disconnected)
            {
                GameModel.Initialize(Network.player.ipAddress, _localPort, GameController.InstructionsDictionaryFilename);
            }
            else if (GameModel.NetworkStatus == NetworkStatus.Connected)
            {
                GameModel.InitializeGame();
            }
            else
            {
                // TODO No players are connected
            }
        }

        public void OnBottomButtonClick()
        {
            if (GameModel.NetworkStatus == NetworkStatus.Disconnected)
            {
                Application.LoadLevel("MainMenu");
            }
            else if (GameModel.NetworkStatus == NetworkStatus.Listening
                || GameModel.NetworkStatus == NetworkStatus.Connected)
            {
                GameModel.Shutdown();
            }
        }

        public void Update()
        {
            if (GameModel.NetworkStatus == NetworkStatus.Disconnected)
            {
                p_localPortField.gameObject.SetActive(true);
                p_topButton.gameObject.SetActive(true);
                p_bottomButton.gameObject.SetActive(true);
                p_serverInformationText.gameObject.SetActive(false);
                p_connectedPlayersText.gameObject.SetActive(false);
                TopButtonText.text = "Create Room";
                BottomButtonText.text = "Back";
                p_statusText.text = "Network server is not running";
            }
            else if (GameModel.NetworkStatus == NetworkStatus.Connecting)
            {
                p_localPortField.gameObject.SetActive(false);
                p_topButton.gameObject.SetActive(false);
                p_bottomButton.gameObject.SetActive(false);
                p_statusText.text = "Network server is starting up...";
            }
            else
            {
                p_localPortField.gameObject.SetActive(false);
                p_topButton.gameObject.SetActive(true);
                p_bottomButton.gameObject.SetActive(true);
                p_serverInformationText.gameObject.SetActive(true);
                p_connectedPlayersText.gameObject.SetActive(true);
                TopButtonText.text = "Play";
                BottomButtonText.text = "Disconnect";
                p_statusText.text = "Network server is running";

                ShowServerInformation();
                ShowPlayersInformation();
            }

            lock(this)
            {
                if (GameInitialized)
                {
                    GameModel.IsHost = true;

                    Application.LoadLevel("GameScene");
                }
            }
        }

        private void ShowServerInformation()
        {
            p_serverInformationText.text = "IP: " + GameModel.NetworkAddress + " Port: " + GameModel.NetworkPort;
        }

        private void ShowPlayersInformation()
        {
            StringBuilder builder = new StringBuilder(512);

            builder.Append("Players (IP/Port):\n");

            foreach (var player in GameModel.NetworkConnections)
            {
                builder.Append(player.Address);
                builder.Append("/");
                builder.Append(player.Port);
                builder.Append("\n");
            }

            p_connectedPlayersText.text = builder.ToString();
        }
    }
}