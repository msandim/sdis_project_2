﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View.Menus
{
    using Model;
    using Model.Net;

    public class JoinRoomMenu : MonoBehaviour
    {
        public InputField p_localPortField;
        public InputField p_remoteIPField;
        public InputField p_remotePortField;
        public Text p_statusText;
        public Button p_topButton;
        public Button p_bottomButton;
        public Text p_playerInformationText;

        private Int32 _localPort;
        private String LocalPort
        {
            get
            {
                return _localPort.ToString();
            }
            set
            {
                Int32.TryParse(value, out _localPort);
            }
        }

        private String RemoteIP { get; set; }

        private Int32 _remotePort;
        private String RemotePort
        {
            get
            {
                return _remotePort.ToString();
            }
            set
            {
                Int32.TryParse(value, out _remotePort);
            }
        }

        private Text TopButtonText { get; set; }
        private Text BottomButtonText { get; set; }

        private GameModel GameModel { get; set; }
        private Boolean GameInitialized { get; set; }

        public void Start()
        {
            RemoteIP = "";
            GameModel = GameController.Instance.GameModel;

            GameModel.GameInitialized += Model_GameInitialized;
            TopButtonText = p_topButton.GetComponentInChildren<Text>();
            BottomButtonText = p_bottomButton.GetComponentInChildren<Text>();
        }

        private void Model_GameInitialized(object sender, Model.Logic.GameLogic.GameInitializedEventArgs e)
        {
            lock (this)
            {
                GameInitialized = true;
            }
        }

        public void OnLocalPortFieldEndEdit(String text)
        {
            LocalPort = p_localPortField.text;
            p_localPortField.text = LocalPort;
        }

        public void OnRemoteIPFieldEndEdit(String text)
        {
            RemoteIP = p_remoteIPField.text;
        }

        public void OnRemotePortFieldEndEdit(String text)
        {
            RemotePort = p_remotePortField.text;
            p_remotePortField.text = RemotePort;
        }

        public void OnTopButtonClick()
        {
            if (GameModel.NetworkStatus == NetworkStatus.Disconnected)
            {
                GameModel.Initialize(Network.player.ipAddress, _localPort, GameController.InstructionsDictionaryFilename);
                GameModel.JoinRoom(RemoteIP, _remotePort);
            }
        }

        public void OnBottomButtonClick()
        {
            if (GameModel.NetworkStatus == NetworkStatus.Disconnected)
            {
                Application.LoadLevel("MainMenu");
            }
            else
            {
                GameModel.Shutdown();
            }
        }

        public void Update()
        {
            if (GameModel.NetworkStatus == NetworkStatus.Disconnected
                || GameModel.NetworkStatus == NetworkStatus.Listening)
            {
                p_localPortField.gameObject.SetActive(true);
                p_remoteIPField.gameObject.SetActive(true);
                p_remotePortField.gameObject.SetActive(true);
                p_topButton.gameObject.SetActive(true);
                p_bottomButton.gameObject.SetActive(true);
                p_playerInformationText.gameObject.SetActive(false);
                TopButtonText.text = "Join Room";
                BottomButtonText.text = "Back";
                p_statusText.text = "Not connected to server";
            }
            else if (GameModel.NetworkStatus == NetworkStatus.Connecting)
            {
                p_localPortField.gameObject.SetActive(false);
                p_remoteIPField.gameObject.SetActive(false);
                p_remotePortField.gameObject.SetActive(false);
                p_topButton.gameObject.SetActive(false);
                p_bottomButton.gameObject.SetActive(false);
                p_statusText.text = "Connecting to server...";
            }
            else
            {
                p_localPortField.gameObject.SetActive(false);
                p_remoteIPField.gameObject.SetActive(false);
                p_remotePortField.gameObject.SetActive(false);
                p_topButton.gameObject.SetActive(false);
                p_bottomButton.gameObject.SetActive(true);
                p_playerInformationText.gameObject.SetActive(true);
                BottomButtonText.text = "Disconnect";
                p_statusText.text = "Connected to server";

                ShowPlayerInformation();
            }

            lock (this)
            {
                if (GameInitialized)
                {
                    GameModel.IsHost = false;

                    Application.LoadLevel("GameScene");
                }
            }
        }

        private void ShowPlayerInformation()
        {
            p_playerInformationText.text = "IP: " + GameModel.NetworkAddress + " Port: " + GameModel.NetworkPort;
        }
    }
}
