﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Assets.Scripts.View
{
    using Model;
    using Model.Logic;

    public class GameScene : MonoBehaviour
    {
        public Text instructionsText;
        public Button[] buttons;
        public Image timeBarImage;

        private GameModel GameModel { get; set; }
        private Boolean HasGameStarted { get; set; }
        private Boolean HasGameEnded { get; set; }
        private HashSet<String> ButtonsText { get; set; }
        private String CurrentInstruction { get; set; }
        private RectTransform TimeBar { get; set; }
        private Single BarWidth { get; set; }
        private Boolean ShowResult { get; set; }

        public void Start()
        {
            GameModel = GameController.Instance.GameModel;
            TimeBar = timeBarImage.rectTransform;
            BarWidth = TimeBar.sizeDelta.x;

            // Initialize buttons:        
            InitializeButtons();

            GameModel.GameStarted += Model_GameStarted;
            GameModel.GameEnded += Model_GameEnded;
            GameModel.InstructionChanged += Model_InstructionChanged;
        }

        private void InitializeButtons()
        {
            HashSet<String> buttonsText = GameModel.CurrentButtons;

            if (buttonsText.Count != buttons.Length)
            {
                Debug.LogError("Buttons' array and names' set differ in size!");
                return;
            }

            Int32 i = 0;
            foreach (String name in buttonsText)
            {
                Button button = buttons[i++];
                Text text = button.GetComponentInChildren<Text>();
                text.text = name;

                button.onClick.AddListener(() =>
                {
                    OnButtonClick(text);
                });
            }
        }

        private void Model_GameStarted(object sender, EventArgs e)
        {
            lock (this)
            {
                HasGameStarted = true;
            }
        }

        private void Model_InstructionChanged(object sender, GameLogic.InstructionsChangedEventArgs e)
        {
            lock (this)
            {
                // Change current instruction:
                CurrentInstruction = e.Name;
            }
        }

        private void Model_GameEnded(object sender, GameLogic.GameEndedEventArgs e)
        {
            lock (this)
            {
                // Show game result:
                CurrentInstruction = e.Won ? "Game Won!" : "Game Lost!";

                HasGameEnded = true;
                ShowResult = true;
            }
        }

        public void OnButtonClick(Text text)
        {
            lock(this)
            {
                if (HasGameEnded)
                {
                    String level = GameModel.IsHost ? "CreateRoomMenu" : "JoinRoomMenu";

                    GameModel.ClearState();
                    Application.LoadLevel(level);
                }
            }

            // Send button clicked message:
            GameModel.ButtonClicked(text.text);
        }

        public void Update()
        {
            lock(this)
            {
                if (!HasGameStarted)
                {
                    TimeSpan timeToStart = GameModel.GameStartTime - DateTime.Now.TimeOfDay;
                    if(timeToStart < TimeSpan.Zero)
                    {
                        if (GameModel.IsGameRunning)
                        {
                            //CurrentInstruction = GameModel.CurrentInstruction;
                            //instructionsText.text = CurrentInstruction;
                            HasGameStarted = true;
                        }
                    }

                    instructionsText.text = timeToStart.ToString();
                }
                else if (!HasGameEnded)
                {
                    if (!GameModel.IsGameRunning)
                        HasGameEnded = true;

                    // Update time bar:
                    UpdateTimeBar();

                    if (CurrentInstruction != instructionsText.text)
                        instructionsText.text = CurrentInstruction;
                }
                else if (ShowResult)
                {
                    ShowResult = false;

                    instructionsText.text = CurrentInstruction;

                    buttons[0].GetComponentInChildren<Text>().text = "Back";

                    // Hide all buttons except first:
                    for (Int32 i = 1; i < buttons.Length; i++)
                    {
                        buttons[i].gameObject.SetActive(false);
                    }

                    // Hide time bar:
                    timeBarImage.gameObject.SetActive(false);
                }
            }
        }

        private void UpdateTimeBar()
        {
            float percentage = 1.0f - ((float)GameModel.InstructionElapsedTime) / ((float)GameModel.InstructionTimeout);
            float currentTimePosition = -(BarWidth / 2.0f) * (1.0f - percentage);

            TimeBar.anchoredPosition = new Vector2(currentTimePosition, TimeBar.anchoredPosition.y);
            TimeBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, percentage * BarWidth);
        }
    }
}