﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Model.Tests
{
    using Net;
    using Net.Sockets;
    using Raft;
    using Raft.State;

    [TestClass()]
    public class GameModelTests
    {
        private static readonly String InstructionsFilename = @"..\..\Resources\InstructionsDictionary.txt";

        private AutoResetEvent _followerWaitHandle;
        private AutoResetEvent _candidateWaitHandle;
        private AutoResetEvent _leaderWaitHandle;
        private AutoResetEvent _waitHandle1;
        private AutoResetEvent _waitHandle2;
        private AutoResetEvent _waitHandle3;

        [TestInitialize()]
        public void InitializeTest()
        {
            _followerWaitHandle = new AutoResetEvent(false);
            _candidateWaitHandle = new AutoResetEvent(false);
            _leaderWaitHandle = new AutoResetEvent(false);
            _waitHandle1 = new AutoResetEvent(false);
            _waitHandle2 = new AutoResetEvent(false);
            _waitHandle3 = new AutoResetEvent(false);
        }

        [TestMethod()]
        public void GameModelRaftNetworkTest()
        {
            GameModel gameModel = new GameModel();
            gameModel.RaftStateChanged += GameModel_RaftStateChanged;

            Assert.AreEqual(NetworkStatus.Disconnected, gameModel.NetworkStatus);

            gameModel.Initialize("127.0.0.2", SocketAddress.UnusedPort, InstructionsFilename);
            Assert.AreEqual(NetworkStatus.Listening, gameModel.NetworkStatus);

            _followerWaitHandle.WaitOne();
            Assert.AreEqual(AbstractState.Type.Follower, gameModel.RaftStatus);

            _candidateWaitHandle.WaitOne();
            Assert.AreEqual(AbstractState.Type.Candidate, gameModel.RaftStatus);

            _leaderWaitHandle.WaitOne();
            Assert.AreEqual(AbstractState.Type.Leader, gameModel.RaftStatus);

            gameModel.Shutdown();

            Assert.AreEqual(NetworkStatus.Disconnected, gameModel.NetworkStatus);
        }

        private void GameModel_RaftStateChanged(object sender, RaftServer.StateChangedEventArgs e)
        {
            switch(e.State)
            {
                case AbstractState.Type.Follower:
                    _followerWaitHandle.Set();
                    break;
                case AbstractState.Type.Candidate:
                    _candidateWaitHandle.Set();
                    break;
                case AbstractState.Type.Leader:
                    _leaderWaitHandle.Set();
                    break;
            }
        }

        [TestMethod()]
        public void GameModelInteractionTest()
        {
            GameModel gameModel1 = new GameModel();
            GameModel gameModel2 = new GameModel();

            gameModel1.PlayerConnected += GameModel1_PlayerConnected;
            gameModel1.PlayerDisconnected += GameModel1_PlayerDisconnected;

            gameModel1.Initialize("127.0.0.3", SocketAddress.UnusedPort, InstructionsFilename);
            gameModel2.Initialize("127.0.0.3", SocketAddress.UnusedPort, InstructionsFilename);

            // Game Model 2 joins room of Game Model 1:
            gameModel2.JoinRoom(gameModel1.NetworkAddress, gameModel1.NetworkPort);
            _waitHandle1.WaitOne();

            gameModel2.ExitRoom();
            _waitHandle2.WaitOne();

            // Join again:
            gameModel2.JoinRoom(gameModel1.NetworkAddress, gameModel1.NetworkPort);
            _waitHandle1.WaitOne();

            // Crash:
            gameModel2.Shutdown();
            _waitHandle2.WaitOne();

            gameModel1.Shutdown();
        }

        private void GameModel1_PlayerConnected(object sender, ServerEventArgs e)
        {
            _waitHandle1.Set();
        }

        private void GameModel1_PlayerDisconnected(object sender, ServerEventArgs e)
        {
            _waitHandle2.Set();
        }
    }
}