﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assets.Scripts.Model.Net;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Model.Net.Tests
{
    using Messages;
    using Sockets;

    [TestClass()]
    public class NetworkManagerTests
    {
        private List<NetworkManager> _networks;
        private List<AutoResetEvent> _waitHandles;
        private Int32 _randomTestMessageID;

        [TestInitialize()]
        public void InitializeTest()
        {
            _networks = new List<NetworkManager>();

            for (Int32 i = 0; i < 3; i++)
            {
                NetworkManager networkManager = new NetworkManager();
                networkManager.Initialize("127.0.0.1", SocketAddress.UnusedPort);
                _networks.Add(networkManager);
            }

            _waitHandles = new List<AutoResetEvent>();
            for (Int32 i = 0; i < 3; i++)
                _waitHandles.Add(new AutoResetEvent(initialState: false));
        }

        [TestCleanup()]
        public void ShutdownTest()
        {
            foreach (var networkManager in _networks)
                networkManager.Shutdown();
        }

        [TestMethod()]
        public void NetworkManagerStatusTest()
        {
            String address1 = _networks[0].Address;
            Int32 port1 = _networks[0].Port;

            Assert.AreEqual(NetworkStatus.Listening, _networks[0].Status);
            Assert.AreEqual(NetworkStatus.Listening, _networks[1].Status);

            _networks[0].ServerConnected += _network0_ServerConnected;
            Assert.IsTrue(_networks[1].ConnectTo(address1, port1));

            // Wait for network2 to connect with network1:
            _waitHandles[0].WaitOne();
            _waitHandles[0].Reset();

            Assert.AreEqual(NetworkStatus.Connected, _networks[0].Status);
            Assert.AreEqual(NetworkStatus.Connected, _networks[1].Status);

            _networks[1].ServerDisconnected += _network1_ServerDisconnected;
            _networks[0].Shutdown();

            // Wait for network2 to realize that network1 "crashed":
            _waitHandles[1].WaitOne();
            _waitHandles[1].Reset();

            Assert.AreEqual(NetworkStatus.Disconnected, _networks[0].Status);
            Assert.AreEqual(NetworkStatus.Listening, _networks[1].Status);

            _networks[0].Initialize(address1, port1);

            Assert.AreEqual(NetworkStatus.Listening, _networks[0].Status);
            Assert.AreEqual(NetworkStatus.Listening, _networks[1].Status);

            _networks[1].ServerConnected += _network1_ServerConnected;
            Assert.IsTrue(_networks[0].ConnectTo(_networks[1].Address, _networks[1].Port));

            // Wait for network1 to connect with network2:
            _waitHandles[1].WaitOne();

            Assert.AreEqual(NetworkStatus.Connected, _networks[0].Status);
            Assert.AreEqual(NetworkStatus.Connected, _networks[1].Status);
        }

        private void _network0_ServerConnected(object sender, ServerEventArgs e)
        {
            _waitHandles[0].Set();
        }

        private void _network1_ServerDisconnected(object sender, ServerEventArgs e)
        {
            _waitHandles[1].Set();
        }

        private void _network1_ServerConnected(object sender, ServerEventArgs e)
        {
            _waitHandles[1].Set();
        }

        [TestMethod()]
        public void NetworkManagerSendTest()
        {
            _networks[0].DataReceived += _network0_DataReceived;
            _networks[1].DataReceived += _network1_DataReceived;
            _networks[1].ServerConnected += _network1_ServerConnected1;

            // Connect network1 to network2:
            Assert.IsTrue(_networks[0].ConnectTo(_networks[1].Address, _networks[1].Port));

            // Wait for connection establishment:
            _waitHandles[0].WaitOne();
            _waitHandles[0].Reset();

            _randomTestMessageID = new Random().Next();
            TestMessage message1 = new TestMessage(_networks[1].ID, _randomTestMessageID);
            Assert.IsTrue(_networks[1].Send(_networks[0].ID, message1.Serialize()));

            // Wait for network1 to receive message:
            _waitHandles[0].WaitOne();
            _waitHandles[0].Reset();

            _randomTestMessageID = new Random().Next();
            TestMessage message2 = new TestMessage(_networks[0].ID, _randomTestMessageID);
            Assert.IsTrue(_networks[0].Send(_networks[1].ID, message2.Serialize()));

            // Wait for network2 to receive message:
            _waitHandles[1].WaitOne();
            _waitHandles[1].Reset();
        }

        private void _network1_ServerConnected1(object sender, ServerEventArgs e)
        {
            _waitHandles[0].Set();
        }

        private void _network0_DataReceived(object sender, DataReceivedEventArgs e)
        {
            TestMessage message = TestMessage.Deserialize(e.Data);

            Assert.AreEqual(_networks[1].ID, message.ServerID);
            Assert.AreEqual(_randomTestMessageID, message.MessageID);

            _waitHandles[0].Set();
        }

        private void _network1_DataReceived(object sender, DataReceivedEventArgs e)
        {
            TestMessage message = TestMessage.Deserialize(e.Data);

            Assert.AreEqual(_networks[0].ID, message.ServerID);
            Assert.AreEqual(_randomTestMessageID, message.MessageID);

            _waitHandles[1].Set();
        }

        [TestMethod()]
        public void NetworkManagerSendAllTest()
        {
            _networks[1].ServerConnected += _network1_ServerConnected2;

            // Connect network0 and network2 to network1:
            Assert.IsTrue(_networks[0].ConnectTo(_networks[1].Address, _networks[1].Port));
            Assert.IsTrue(_networks[2].ConnectTo(_networks[1].Address, _networks[1].Port));

            // Wait for connection establishment:
            _waitHandles[1].WaitOne();
            _waitHandles[1].WaitOne();

            _networks[0].DataReceived += _network0_DataReceived1;
            _networks[2].DataReceived += _network2_DataReceived;

            TestMessage message = new TestMessage(_networks[1].ID, 5000);
            _networks[1].SendAll(message.Serialize());

            _waitHandles[0].WaitOne();
            _waitHandles[2].WaitOne();
        }

        private void _network1_ServerConnected2(object sender, ServerEventArgs e)
        {
            _waitHandles[1].Set();
        }

        private void _network0_DataReceived1(object sender, DataReceivedEventArgs e)
        {
            _waitHandles[0].Set();
        }

        private void _network2_DataReceived(object sender, DataReceivedEventArgs e)
        {
            _waitHandles[2].Set();
        }
        [TestMethod()]
        public void NetworkManagerNetworkSetupTest()
        {
            // Add two more networks:
            for (Int32 i = 0; i < 2; i++)
            {
                NetworkManager network = new NetworkManager();
                network.Initialize("127.0.0.1", SocketAddress.UnusedPort);
                _networks.Add(network);

                _waitHandles.Add(new AutoResetEvent(initialState: false));
            }

            foreach (var network in _networks)
                network.ServerConnected += Network_ServerConnected;

            // Network 1 connects to Network 0:
            _networks[1].ConnectTo(_networks[0].Address, _networks[0].Port);

            _waitHandles[0].WaitOne();

            // Network 2 connects to Network 0:
            _networks[2].ConnectTo(_networks[0].Address, _networks[0].Port);

            _waitHandles[0].WaitOne();
            _waitHandles[0].Reset();
            _waitHandles[1].WaitOne();
            _waitHandles[1].Reset();

            // Network 3 connects to Network 1:
            _networks[3].ConnectTo(_networks[1].Address, _networks[1].Port);

            _waitHandles[0].WaitOne();
            _waitHandles[0].Reset();
            _waitHandles[1].WaitOne();
            _waitHandles[1].Reset();
            _waitHandles[2].WaitOne();
            _waitHandles[2].Reset();

            // Check all connections:
            for (Int32 i = 0; i < 4; i++)
                Assert.IsTrue(CheckNetworkConnections(i));
        }

        private void Network_ServerConnected(object sender, ServerEventArgs e)
        {
            Int32 index = _networks.FindIndex((NetworkManager network) =>
            {
                return network == sender;
            });

            _waitHandles[index].Set();
        }

        private Boolean CheckNetworkConnections(Int32 index)
        {
            String networkID = _networks[index].ID;

            for(Int32 i = 0; i < 4; i++)
            {
                if (i == index)
                    continue;

                if (!_networks[i].ContainsServer(networkID))
                    return false;
            }

            return true;
        }
    }
}