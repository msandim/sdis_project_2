﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;

namespace Assets.Scripts.Model.Net.Messages.Tests
{
    [TestClass()]
    public class MessageTests
    {
        [TestMethod()]
        public void FindTypeTest()
        {
            TestMessage testMessage = new TestMessage("", 0);
            String json = JsonConvert.SerializeObject(testMessage);

            String type;
            Assert.IsTrue(Message.FindType(json, out type));

            Assert.AreEqual(TestMessage.StaticType, type);
        }

        [TestMethod()]
        public void SerializeTest()
        {
            TestMessage testMessage = new TestMessage("", 0);
            String json = JsonConvert.SerializeObject(testMessage);
            String json2 = testMessage.Serialize();

            Assert.AreEqual(json, json2);
        }
    }
}