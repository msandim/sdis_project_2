﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Assets.Scripts.Model.Net.Sockets.Tests
{
    using Messages;

    [TestClass()]
    public class TcpSenderSocketTests
    {
        private enum TestId : int
        {
            DoNothing = 0,
            ReplyBack = 1
        };

        private static String _address;
        private static Int32 _timeout;

        private Connection _connection;
        private TcpListenerSocket _connectionsListener;
        private TcpSenderSocket _remoteSocket;
        private AutoResetEvent _waitHandle;
        private AutoResetEvent _waitHandle2;
        private TcpSenderSocket _sender;

        [ClassInitialize()]
        public static void InitializeClass(TestContext context)
        {
            _address = SocketAddress.LocalIP;
            _timeout = 2000;
        }

        [TestInitialize()]
        public void InitializeTest()
        {
            _waitHandle = new AutoResetEvent(false);
            _waitHandle2 = new AutoResetEvent(false);
            _connection = new Connection(_address, SocketAddress.UnusedPort);

            // Initialize listener socket:
            _connectionsListener = new TcpListenerSocket();
            _connectionsListener.SocketConnected += ConnectionsListener_SocketConnected;
            _connectionsListener.Initialize(_address, _connection.Port);

            // Start listening for connections:
            _connectionsListener.StartListening();

            // Initialize a remote socket (a connection is requested):
            _remoteSocket = new TcpSenderSocket();
            _remoteSocket.DataReceived += RemoteSocket_DataReceived;
            Assert.IsTrue(_remoteSocket.Initialize(_connection));
        }

        [TestCleanup()]
        public void ShutdownTest()
        {
            // Shutdown remote socket:
            _remoteSocket.Shutdown();

            // Shutdown connections listener:
            _connectionsListener.StopListening();
            _connectionsListener.Shutdown();
        }

        [TestMethod()]
        public void DoNothingTest()
        {
            // Send a test message:
            TestMessage message = new TestMessage("server", (Int32)TestId.DoNothing);
            _remoteSocket.SendData(message.Serialize());

            // Wait for a signal that data was received:
            if (!_waitHandle.WaitOne(_timeout))
                Assert.Fail("Wait Handle timeout!");
        }

        [TestMethod()]
        public void ReplyBackTest()
        {
            // Start listening on remote socket:
            _remoteSocket.StartListening();

            // Send a test message:
            TestMessage message = new TestMessage("server", (Int32)TestId.ReplyBack);
            _remoteSocket.SendData(message.Serialize());

            _waitHandle.WaitOne(2000);

            Assert.IsNotNull(_sender);

            // Send reply to remote socket:
            TestMessage reply = new TestMessage("server", (Int32)TestId.DoNothing);
            _sender.SendData(reply.Serialize());

            // Wait for a signal that data was received:
            if (!_waitHandle2.WaitOne(2000))
                Assert.Fail("Wait Handle timeout!");

            // Shutdown local socket:
            _sender.StopListening();
            _sender.Shutdown();
        }

        private void ConnectionsListener_SocketConnected(object sender, TcpListenerSocket.SocketConnectedEventArgs e)
        {
            // The connected socket must be connected:
            Assert.IsTrue(e.SocketHandler.Connected);

            // Initialize local socket using the connected socket:
            TcpSenderSocket localSocket = new TcpSenderSocket();
            localSocket.DataReceived += LocalSocket_DataReceived;
            localSocket.Initialize(e.SocketHandler);

            // Start listening for incoming data:
            localSocket.StartListening();
        }

        private void LocalSocket_DataReceived(TcpSenderSocket sender, TcpSenderSocket.StateChangedEventArgs e)
        {
            String data = e.State.Data;

            // Check if it is a test message:
            String type;
            Assert.IsTrue(Message.FindType(data, out type));
            Assert.AreEqual(TestMessage.StaticType, type);

            _sender = sender;

            // Signal that data was received:
            _waitHandle.Set();
        }

        private void RemoteSocket_DataReceived(TcpSenderSocket sender, TcpSenderSocket.StateChangedEventArgs e)
        {
            String data = e.State.Data;

            // Check if it is a test message:
            String type;
            Assert.IsTrue(Message.FindType(data, out type));
            Assert.AreEqual(TestMessage.StaticType, type);

            // Check test message ID:
            TestMessage message = TestMessage.Deserialize(data);
            if (message.MessageID == (Int32)TestId.DoNothing)
            {
                // Signal that the reply was received:
                _waitHandle2.Set();
            }
        }
    }
}