﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Sockets;
using System;
using System.Threading;

namespace Assets.Scripts.Model.Net.Sockets.Tests
{
    using Messages;

    [TestClass()]
    public class TcpListenerSocketTests
    {
        private Socket _receivedSocketHandler;
        private ManualResetEvent _waitEvent;

        [TestMethod()]
        public void TcpListenerSocketTest()
        {
            _waitEvent = new ManualResetEvent(false);
            String address = SocketAddress.LocalIP;
            Int32 port = SocketAddress.UnusedPort;

            // Initialize listener socket:
            TcpListenerSocket listener = new TcpListenerSocket();
            listener.Initialize(address, port);
            listener.SocketConnected += ListenerSocket_SocketConnected;

            // Start listener socket:
            listener.StartListening();

            // Set to nonsignaled state:
            _waitEvent.Reset();

            // Initialize sender socket:
            TcpSenderSocket sender = new TcpSenderSocket();
            Connection senderConnection = new Connection(address, port);
            sender.Initialize(senderConnection);

            // Wait for signal, but using a timeout:
            _waitEvent.WaitOne(500);

            // Shutdown sender socket:
            sender.Shutdown();

            // Assert that the socket was received and that it is connected:
            Assert.IsNotNull(_receivedSocketHandler);
            Assert.IsTrue(_receivedSocketHandler.Connected);

            // Stop listener socket:
            listener.StopListening();

            // Shutdown listener socket:
            listener.Shutdown();
        }

        private void ListenerSocket_SocketConnected(object sender, TcpListenerSocket.SocketConnectedEventArgs e)
        {
            _receivedSocketHandler = e.SocketHandler;
            _waitEvent.Set();
        }
    }
}