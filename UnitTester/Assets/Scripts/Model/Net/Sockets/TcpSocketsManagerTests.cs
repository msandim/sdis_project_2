﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Assets.Scripts.Model.Net.Sockets.Tests
{
    using Messages;

    [TestClass()]
    public class TcpSocketsManagerTests
    {
        private enum TestId
        {
            Increment = 0,
            Incremented = 1,
            SendAll1 = 2, 
            SendAll2 = 3,
            SendAll3 = 4
        }

        private static String _address;

        private Connection _connection1;
        private Connection _connection2;
        private TcpSocketsManager _socketManager1;
        private TcpSocketsManager _socketManager2;
        private TcpSocketsManager _socketManager3;
        private ManualResetEvent _waitHandle;
        private ManualResetEvent _waitHandle2;

        [ClassInitialize()]
        public static void InitializeClass(TestContext context)
        {
            _address = SocketAddress.LocalIP;
        }

        [TestInitialize()]
        public void InitializeTest()
        {
            _connection1 = new Connection(_address, SocketAddress.UnusedPort);
            _connection2 = new Connection(_address, SocketAddress.UnusedPort);
            _waitHandle = new ManualResetEvent(false);
            _waitHandle2 = new ManualResetEvent(false);

            // Initialize socket managers on a specific port:
            _socketManager1 = new TcpSocketsManager();
            _socketManager1.Initialize(_address, _connection1.Port);
            _socketManager2 = new TcpSocketsManager();
            _socketManager2.Initialize(_address, _connection2.Port);

            // Create a socket on a specific address and port:
            _socketManager1.DataReceived += SocketManager1_DataReceived;
            _socketManager2.DataReceived += SocketManager2_DataReceived;
            Assert.IsTrue(_socketManager1.CreateSocket(_connection2));
        }

        [TestCleanup()]
        public void ShutdownTest()
        {
            _socketManager2.Shutdown();
            _socketManager1.Shutdown();
        }

        [TestMethod()]
        public void SendReceiveTest()
        {
            // Send an increment ID message:
            TestMessage sentMessage = new TestMessage("server", (Int32) TestId.Increment);
            _socketManager1.Send(_connection2, sentMessage.Serialize());

            // Wait for reply:
            _waitHandle.WaitOne();
        }

        [TestMethod()]
        public void SendAllTest()
        {
            // Initialize a third socket manager:
            _socketManager3 = new TcpSocketsManager();
            _socketManager3.DataReceived += SocketManager3_DataReceived;
            Connection connection3 = new Connection(_address, SocketAddress.UnusedPort);
            _socketManager3.Initialize(_address, connection3.Port);

            // Create a second socket in socket manager 1:
            Assert.IsTrue(_socketManager1.CreateSocket(connection3));

            // Send an increment ID message:
            TestMessage sentMessage = new TestMessage("server", (Int32)TestId.SendAll1);
            _socketManager1.SendAll(sentMessage.Serialize());

            // Wait for 2 reply:
            _waitHandle.WaitOne();
            _waitHandle.WaitOne();

            _socketManager3.Shutdown();
        }

        private void SocketManager1_DataReceived(object sender, TcpSocketsManager.DataReceivedEventArgs e)
        {
            String data = e.Data;

            // Check if data received corresponds to a test message:
            String type;
            Assert.IsTrue(Message.FindType(data, out type));
            Assert.AreEqual(TestMessage.StaticType, type);

            TestMessage receivedMessage = TestMessage.Deserialize(data);
            if (receivedMessage.MessageID == (Int32)TestId.Incremented)
            {
                // Signal that message was succesfully received:
                _waitHandle.Set();
            }
            else if(receivedMessage.MessageID == (Int32) TestId.SendAll2)
            {
                _waitHandle.Set();
            }
            else if(receivedMessage.MessageID == (Int32) TestId.SendAll3)
            {
                _waitHandle2.Set();
            }
            else
            {
                Assert.Fail("Message was not correctly interpretated");
            }
        }

        private void SocketManager2_DataReceived(object sender, TcpSocketsManager.DataReceivedEventArgs e)
        {
            String data = e.Data;

            // Check if data received corresponds to a test message:
            String type;
            Assert.IsTrue(Message.FindType(data, out type));
            Assert.AreEqual(TestMessage.StaticType, type);

            TestMessage receivedMessage = TestMessage.Deserialize(data);
            TestMessage replyMessage;
            if (receivedMessage.MessageID == (Int32) TestId.Increment)
            {
                // Increment message ID:
                replyMessage = new TestMessage("server", receivedMessage.MessageID + 1);
            }
            else if(receivedMessage.MessageID == (Int32) TestId.SendAll1)
            {
                // Increment message ID:
                replyMessage = new TestMessage("server", (Int32) TestId.SendAll2);
            }
            else
            {
                Assert.Fail("Message ID not expected");
                return;
            }

            // Send message back:
            _socketManager2.Send(e.Connection, replyMessage.Serialize());
        }

        private void SocketManager3_DataReceived(object sender, TcpSocketsManager.DataReceivedEventArgs e)
        {
            String data = e.Data;

            // Check if data received corresponds to a test message:
            String type;
            Assert.IsTrue(Message.FindType(data, out type));
            Assert.AreEqual(TestMessage.StaticType, type);

            TestMessage receivedMessage = TestMessage.Deserialize(data);
            if (receivedMessage.MessageID == (Int32)TestId.SendAll1)
            {
                // Increment message ID:
                TestMessage replyMessage = new TestMessage("server", (Int32) TestId.SendAll3);

                // Send message back:
                _socketManager3.Send(e.Connection, replyMessage.Serialize());
            }
            else
            {
                Assert.Fail("Message ID not expected");
            }
        }
    }
}