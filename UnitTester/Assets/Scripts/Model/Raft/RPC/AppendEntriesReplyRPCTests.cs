﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Raft.RPC.Tests
{
    [TestClass()]
    public class AppendEntriesReplyRPCTests
    {
        [TestMethod()]
        public void AppendEntriesReplyRPCTest()
        {
            Int32 term = 1;
            Boolean success = true;
            Int32 lastLogIndex = 2;
            AppendEntriesReplyRPC message = new AppendEntriesReplyRPC(term, success, lastLogIndex);

            String json = message.Serialize();
            AppendEntriesReplyRPC convertedMessage = AppendEntriesReplyRPC.Deserialize(json);

            Assert.AreEqual(term, convertedMessage.Term);
            Assert.AreEqual(success, convertedMessage.Success);
            Assert.AreEqual(lastLogIndex, convertedMessage.LastLogIndex);
        }
    }
}