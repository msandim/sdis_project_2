﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Raft.RPC.Tests
{
    [TestClass()]
    public class RequestVoteRPCTests
    {
        [TestMethod()]
        public void RequestVoteRPCTest()
        {
            String candidateID = "Peer1";
            Int32 term = 1;
            Int32 lastLogIndex = 2;
            Int32 lastLogTerm = 3;
            RequestVoteRPC message = new RequestVoteRPC(candidateID, term, lastLogIndex, lastLogTerm);

            String json = message.Serialize();
            RequestVoteRPC convertedMessage = RequestVoteRPC.Deserialize(json);

            Assert.AreEqual(candidateID, convertedMessage.CandidateID);
            Assert.AreEqual(term, convertedMessage.Term);
            Assert.AreEqual(lastLogIndex, convertedMessage.LastLogIndex);
            Assert.AreEqual(lastLogTerm, convertedMessage.LastLogTerm);
        }
    }
}