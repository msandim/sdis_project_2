﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Raft.RPC.Tests
{
    [TestClass()]
    public class ReplyVoteRPCTests
    {
        [TestMethod()]
        public void ReplyVoteRPCTest()
        {
            Int32 term = 1;
            Boolean voteGranted = true;
            RequestVoteReplyRPC message = new RequestVoteReplyRPC(term, voteGranted);

            String json = message.Serialize();
            RequestVoteReplyRPC convertedMessage = RequestVoteReplyRPC.Deserialize(json);

            Assert.AreEqual(term, convertedMessage.Term);
            Assert.AreEqual(voteGranted, convertedMessage.VoteGranted);
        }
    }
}