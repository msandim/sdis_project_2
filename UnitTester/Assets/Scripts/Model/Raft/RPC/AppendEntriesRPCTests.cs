﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Raft.RPC.Tests
{
    [TestClass()]
    public class AppendEntriesRPCTests
    {
        [TestMethod()]
        public void AppendEntriesRPCTest()
        {
            Int32 term = 1;
            String leaderID = "Peer1";
            Int32 previousLogIndex = 2;
            Int32 previousLogTerm = 3;
            Int32 commitIndex = 4;
            AppendEntriesRPC message = new AppendEntriesRPC(term, leaderID, previousLogIndex, previousLogTerm, commitIndex);

            // Add log entries:
            Int32 term0 = 5;
            Int32 index0 = 6;
            String command0 = "Olaf";
            LogEntry entry0 = new LogEntry(term0, index0, command0);

            Int32 term1 = 7;
            Int32 index1 = 8;
            String command1 = "Olaf2";
            LogEntry entry1 = new LogEntry(term1, index1, command1);

            message.AddEntry(entry0);
            message.AddEntry(entry1);
            Int32 numberEntries = message.Entries.Count;

            String json = message.Serialize();
            AppendEntriesRPC convertedMessage = AppendEntriesRPC.Deserialize(json);

            Assert.AreEqual(term, convertedMessage.Term);
            Assert.AreEqual(leaderID, convertedMessage.LeaderID);
            Assert.AreEqual(previousLogIndex, convertedMessage.PreviousLogIndex);
            Assert.AreEqual(previousLogTerm, convertedMessage.PreviousLogTerm);
            Assert.AreEqual(commitIndex, convertedMessage.CommitIndex);

            List<LogEntry> entries = convertedMessage.Entries;
            Assert.AreEqual(numberEntries, entries.Count);

            LogEntry convertedEntry0 = entries[0];
            Assert.AreEqual(term0, convertedEntry0.Term);
            Assert.AreEqual(index0, convertedEntry0.Index);
            Assert.AreEqual(command0, convertedEntry0.Command);

            LogEntry convertedEntry1 = entries[1];
            Assert.AreEqual(term1, convertedEntry1.Term);
            Assert.AreEqual(index1, convertedEntry1.Index);
            Assert.AreEqual(command1, convertedEntry1.Command);
        }
    }
}