﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assets.Scripts.Model.Raft.State;
using System;
using System.Threading;

namespace Assets.Scripts.Model.Raft.State.Tests
{
    using Net;
    using Net.Sockets;

    [TestClass()]
    public class CandidateStateTests
    {
        private NetworkTester _network1;
        private NetworkTester _network2;
        private AutoResetEvent _newLeaderWaitHandle;
        private AutoResetEvent _newFollowerWaitHandle;
        private volatile Boolean _fail;

        [TestInitialize()]
        public void InitializeTest()
        {
            _newLeaderWaitHandle = new AutoResetEvent(false);
            _newFollowerWaitHandle = new AutoResetEvent(false);

            _network1 = new NetworkTester();
            _network1.DataSent += Network1_DataSent;
            Connection connection1 = new Connection("127.0.0.1", 25000);
            _network1.Initialize(connection1.Address, connection1.Port);

            _network2 = new NetworkTester();
            _network2.DataSent += Network2_DataSent;
            Connection connection2 = new Connection("127.0.0.1", 25001);
            _network2.Initialize(connection2.Address, connection2.Port);

            _network1.AddServer(connection2.ID, connection2);
            _network2.AddServer(connection1.ID, connection1);
        }

        private void Network1_DataSent(object sender, NetworkTester.DataSentEventArgs e)
        {
            // Data sent from network1 is redirected to network2:
            _network2.SendTestMessage(e.From, e.Data);
        }

        private void Network2_DataSent(object sender, NetworkTester.DataSentEventArgs e)
        {
            // Data sent from network2 is redirected to network1:
            _network1.SendTestMessage(e.From, e.Data);
        }

        [TestMethod()]
        public void CandidateFollowerTest()
        {
            FollowerState follower = new FollowerState();
            follower.Initialize(new PersistentState(_network1.ID), _network1, CandidateFollower1_ChangeState);
            CandidateState candidate = new CandidateState();
            candidate.Initialize(new PersistentState(_network2.ID), _network2, CandidateFollower2_ChangeState);

            _newLeaderWaitHandle.WaitOne();

            if (_fail)
                Assert.Fail();
        }

        private void CandidateFollower1_ChangeState(AbstractState.Type type, PersistentState state)
        {
            _fail = true;
            _newLeaderWaitHandle.Set();
        }

        private void CandidateFollower2_ChangeState(AbstractState.Type type, PersistentState state)
        {
            if (type == AbstractState.Type.Leader)
                _newLeaderWaitHandle.Set();
        }
    }
}