﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Model.Raft.State.Tests
{
    using Net;
    using Net.Messages;
    using Net.Sockets;
    using RPC;

    [TestClass()]
    public class FollowerStateTests
    {
        private FollowerState _follower;
        private NetworkTester _network;
        private ManualResetEvent _waitHandle;
        private List<LogEntry> _testLog;

        [TestInitialize()]
        public void InitializeTest()
        {
            _follower = new FollowerState();
            _network = new NetworkTester();
            _waitHandle = new ManualResetEvent(initialState: false);

            _network.Initialize("127.0.0.1", SocketAddress.UnusedPort);

            _testLog = new List<LogEntry>
                {
                    new LogEntry(term: 0, index: 0, command: "Button0"),
                    new LogEntry(term: 0, index: 1, command: "Button1"),
                    new LogEntry(term: 1, index: 2, command: "Button2"),
                    new LogEntry(term: 1, index: 3, command: "Button3"),
                    new LogEntry(term: 1, index: 4, command: "Button4"),
                };
        }

        private void SendMessageAndCompareLogs(PersistentState state, Int32 term, Int32 previousLogIndex, Int32 previousLogTerm, Int32 commitIndex, Int32 begin, Int32 end)
        {
            _follower.Initialize(state, _network, DoNothingChangeState);

            SendAppendEntriesMessage(term, previousLogIndex, previousLogTerm, commitIndex, begin, end);

            Thread.Sleep(500);

            List<LogEntry> followerLog = _follower.PersistentState.Log;
            CompareLogs(_testLog, followerLog);
        }

        private void SendAppendEntriesMessage(Int32 term, Int32 previousLogIndex, Int32 previousLogTerm, Int32 commitIndex, Int32 begin, Int32 end)
        {
            AppendEntriesRPC message = new AppendEntriesRPC(term, "Leader", previousLogIndex, previousLogTerm, commitIndex);
            for (Int32 i = begin; i < end; i++)
                message.AddEntry(_testLog[i]);

            _network.SendTestMessage(String.Empty, message.Serialize());
        }

        private void CompareLogs(List<LogEntry> log1, List<LogEntry> log2)
        {
            Assert.AreEqual(log1.Count, log2.Count);

            // Compare each entry:
            for (Int32 i = 0; i < log1.Count; i++)
            {
                LogEntry entry1 = log1[i];
                LogEntry entry2 = log2[i];

                // Compare term, index and command:
                Assert.AreEqual(entry1.Term, entry2.Term);
                Assert.AreEqual(entry1.Index, entry2.Index);
                Assert.AreEqual(entry1.Command, entry2.Command);

                Assert.IsTrue(entry1 == entry2);
            }
        }

        /// <summary>
        /// The followers log is empty.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest1()
        {
            SendMessageAndCompareLogs(
                state: new PersistentState(_network.ID),
                term: 1,
                previousLogIndex: -1,
                previousLogTerm: -1,
                commitIndex: -1,
                begin: 0,
                end: _testLog.Count
                );
        }


        /// <summary>
        /// The followers has some entries, but not all.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest2()
        {
            PersistentState state = new PersistentState(_network.ID);
            for (Int32 i = 0; i < 3; i++)
                state.AddLogEntry(_testLog[i]);

            SendMessageAndCompareLogs(
                state: new PersistentState(_network.ID),
                term: 1,
                previousLogIndex: -1,
                previousLogTerm: -1,
                commitIndex: -1,
                begin: 0,
                end: _testLog.Count
                );
        }

        /// <summary>
        /// The followers log has more entries than leader.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest3()
        {
            PersistentState state = new PersistentState(_network.ID);
            for (Int32 i = 0; i < _testLog.Count; i++)
                state.AddLogEntry(_testLog[i]);

            state.AddLogEntry(new LogEntry(5, state.Log.Count, "buttonX"));
            state.AddLogEntry(new LogEntry(5, state.Log.Count, "buttonY"));
            state.AddLogEntry(new LogEntry(6, state.Log.Count, "buttonZ"));
            state.AddLogEntry(new LogEntry(7, state.Log.Count, "buttonS"));

            SendMessageAndCompareLogs(
                state: new PersistentState(_network.ID),
                term: 1,
                previousLogIndex: -1,
                previousLogTerm: -1,
                commitIndex: -1,
                begin: 0,
                end: _testLog.Count
                );
        }

        /// <summary>
        /// The follower has the same log as the leaders.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest4()
        {
            PersistentState state = new PersistentState(_network.ID);
            for (Int32 i = 0; i < _testLog.Count; i++)
                state.AddLogEntry(_testLog[i]);

            SendMessageAndCompareLogs(
                state: new PersistentState(_network.ID),
                term: 1,
                previousLogIndex: -1,
                previousLogTerm: -1,
                commitIndex: -1,
                begin: 0,
                end: _testLog.Count
                );
        }

        /// <summary>
        /// The follower has incongruent entries.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest5()
        {
            PersistentState state = new PersistentState(_network.ID);
            for (Int32 i = 0; i < 3; i++)
                state.AddLogEntry(_testLog[i]);

            state.AddLogEntry(new LogEntry(5, state.Log.Count, "buttonX"));
            state.AddLogEntry(new LogEntry(5, state.Log.Count, "buttonY"));
            state.AddLogEntry(new LogEntry(6, state.Log.Count, "buttonZ"));
            state.AddLogEntry(new LogEntry(7, state.Log.Count, "buttonS"));

            SendMessageAndCompareLogs(
                state: new PersistentState(_network.ID),
                term: 1,
                previousLogIndex: -1,
                previousLogTerm: -1,
                commitIndex: -1,
                begin: 0,
                end: _testLog.Count
                );
        }

        /// <summary>
        /// The follower has incongruent entries.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest6()
        {
            PersistentState state = new PersistentState(_network.ID);
            state.AddLogEntry(_testLog[0]);

            _follower.Initialize(state, _network, DoNothingChangeState);

            SendAppendEntriesMessage(1, 0, 0, 0, 1, 2);
        }

        /// <summary>
        /// Test if heartbeat overides current follower state.
        /// </summary>
        [TestMethod()]
        public void FollowerStateTest7()
        {
            PersistentState state = new PersistentState(_network.ID);
            for (Int32 i = 0; i < _testLog.Count; i++)
                state.AddLogEntry(_testLog[i]);

            _follower.Initialize(state, _network, DoNothingChangeState);

            SendAppendEntriesMessage(
                1,
                _testLog.Count - 1,
                _testLog[_testLog.Count - 1].Term,
                3,
                0,
                0
                );

            CompareLogs(_testLog, _follower.PersistentState.Log);
        }

        public void DoNothingChangeState(AbstractState.Type type, PersistentState state)
        {
        }
    }
}
