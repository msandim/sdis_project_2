﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Model.Raft.State.Tests
{
    using RPC;
    using Net;
    using Net.Messages;
    using Net.Sockets;

    [TestClass()]
    public class LeaderStateTests
    {
        private LeaderState _leader;
        private FollowerState _follower;
        private NetworkTester _networkTester;
        private NetworkManager _leaderNetwork;
        private NetworkManager _followerNetwork;
        private AutoResetEvent _waitHandle;
        private AutoResetEvent _waitHandle2;

        [TestInitialize()]
        public void InitializeTest()
        {
            _leader = new LeaderState();
            _networkTester = new NetworkTester();
            _waitHandle = new AutoResetEvent(initialState: false);
            _waitHandle2 = new AutoResetEvent(initialState: false);

            _networkTester.Initialize("127.0.0.1", 25000);
        }

        [TestMethod()]
        public void LeaderStepDownTest()
        {
            // Initialize leader:
            _leader.Initialize(new PersistentState(_networkTester.ID), _networkTester, LeaderStepDownTest_ChangeState);

            // Send an Append Entry with a higher term:
            AppendEntriesRPC message = new AppendEntriesRPC(1, "leader1", -1, -1, -1);
            _networkTester.SendTestMessage(String.Empty, message.Serialize());

            // Wait for signal:
            _waitHandle.WaitOne();

            // Shutdown leader:
            _leader.Shutdown();
        }

        private void LeaderStepDownTest_ChangeState(AbstractState.Type type, PersistentState persistentState)
        {
            Assert.AreEqual(AbstractState.Type.Follower, type);

            // Signal that leader stepped down:
            _waitHandle.Set();
        }

        [TestMethod()]
        public void LeaderEmptyHeartBeatTest()
        {
            Connection[] connections =
            {
                new Connection("127.0.0.1", SocketAddress.UnusedPort),
                new Connection("127.0.0.1", SocketAddress.UnusedPort),
                new Connection("127.0.0.1", SocketAddress.UnusedPort),
                new Connection("127.0.0.1", SocketAddress.UnusedPort),
            };

            foreach (var connection in connections)
                _networkTester.AddServer(connection.ID, connection);
            
            // Add delegate to data sent from leader:
            _networkTester.DataSent += LeaderEmptyHeatBeatTest_DataSent;

            // Initialize leader:
            _leader.Initialize(new PersistentState(_networkTester.ID), _networkTester, null);

            for (Int32 i = 0; i < 4; i++)
                _waitHandle.WaitOne();

            // Shutdown leader:
            _leader.Shutdown();
        }

        private void LeaderEmptyHeatBeatTest_DataSent(object sender, NetworkTester.DataSentEventArgs e)
        {
            // Signal that heart beat was received:
            _waitHandle.Set();
        }

        [TestMethod()]
        public void LeaderCommitIndexTest()
        {
            Connection[] followers =
            {
                new Connection("127.0.0.1", 25000),
                new Connection("127.0.0.1", 25001),
                new Connection("127.0.0.1", 25002),
                new Connection("127.0.0.1", 25003),
                new Connection("127.0.0.1", 25004),
                new Connection("127.0.0.1", 25005),
            };

            LogEntry[] logEntries =
            {
                new LogEntry(term: 0, index: 0, command: "command0"),
                new LogEntry(term: 0, index: 1, command: "command1"),
                new LogEntry(term: 1, index: 2, command: "command2"),
                new LogEntry(term: 1, index: 3, command: "command3"),
                new LogEntry(term: 2, index: 4, command: "command4"),
                new LogEntry(term: 2, index: 5, command: "command5"),
            };

            // Add server IDs to net manager:
            foreach (var follower in followers)
                _networkTester.AddServer(follower.ID, follower);

            // Create state:
            PersistentState state = new PersistentState(_networkTester.ID);
            foreach (var entry in logEntries)
                state.AddLogEntry(entry);

            state.CommitIndex = 2;
            state.CurrentTerm = 3;

            // Initialize leader:
            _networkTester.DataSent += LeaderCommitIndexTest_DataSent;
            _leader.StateChanged += LeaderCommitIndexTest_StateChanged;
            _leader.Initialize(state, _networkTester, null);

            AppendEntriesReplyRPC[] messages =
            {
                new AppendEntriesReplyRPC(3, true, lastLogIndex: 3),
                new AppendEntriesReplyRPC(3, true, lastLogIndex: 3),
                new AppendEntriesReplyRPC(3, true, lastLogIndex: 3),
                new AppendEntriesReplyRPC(3, true, lastLogIndex: 3),
                new AppendEntriesReplyRPC(3, true, lastLogIndex: 4),
                new AppendEntriesReplyRPC(3, true, lastLogIndex: 5),
            };

            foreach (var message in messages)
                _networkTester.SendTestMessage("", message.Serialize());

            _waitHandle.WaitOne();
            _waitHandle2.WaitOne();

            _leader.Shutdown();
        }

        private void LeaderCommitIndexTest_DataSent(object sender, NetworkTester.DataSentEventArgs e)
        {
            String data = e.Data;
            String type;

            // Ignore mal formed messages:
            if (!Message.FindType(data, out type))
                return;

            if (AppendEntriesRPC.StaticType == type)
            {
                var message = AppendEntriesRPC.Deserialize(e.Data);

                if (message.CommitIndex == 3)
                    _waitHandle.Set();
            }
        }

        private void LeaderCommitIndexTest_StateChanged(object sender, PersistentState.MachineStateChangedEventArgs e)
        {
            if (e.Command == "command3")
            {
                _waitHandle2.Set();
            }
        }

        [TestMethod()]
        public void LeaderOldAppendEntriesTest()
        {
            _leaderNetwork = new NetworkManager();
            _leaderNetwork.ServerConnected += _leaderNetwork_ServerConnected;
            _leaderNetwork.Initialize("127.0.0.1", SocketAddress.UnusedPort);

            PersistentState persistentState = new PersistentState(_leaderNetwork.ID);
            _leader.Initialize(persistentState, _leaderNetwork, LeaderOldAppendEntriesTest_ChangeState);

            _leader.HandleClientRequest("command0");
            _leader.HandleClientRequest("command1");
            _leader.HandleClientRequest("command2");

            Thread.Sleep(500);

            // Entries must be committed as there are no followers:
            //Assert.AreEqual(2, _leader.PersistentState.CommitIndex);

            // Initialize a new follower:
            _followerNetwork = new NetworkManager();
            _followerNetwork.Initialize("127.0.0.1", SocketAddress.UnusedPort);
            _followerNetwork.ConnectTo(_leaderNetwork.Address, _leaderNetwork.Port);

            _waitHandle.WaitOne();

            _follower = new FollowerState();
            _follower.StateChanged += Follower_StateChanged;
            _follower.Initialize(new PersistentState(_followerNetwork.ID), _followerNetwork, LeaderOldAppendEntriesTest_ChangeState);

            _waitHandle2.WaitOne();

            _follower.Shutdown();
            _followerNetwork.Shutdown();
            _leader.Shutdown();
            _leaderNetwork.Shutdown();
        }

        private void Follower_StateChanged(object sender, PersistentState.MachineStateChangedEventArgs e)
        {
            if(_follower.PersistentState.LogCount == 3)
                _waitHandle2.Set();
        }

        private void _leaderNetwork_ServerConnected(object sender, ServerEventArgs e)
        {
            _waitHandle.Set();
        }

        private void LeaderOldAppendEntriesTest_ChangeState(AbstractState.Type type, PersistentState persistentState)
        {
            // Do nothing
        }

    }
}